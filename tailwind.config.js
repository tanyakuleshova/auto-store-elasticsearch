const defaultTheme = require('tailwindcss/defaultTheme');
module.exports = {
    content: [
        './storage/framework/views/*.php',
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './config/*.php'
    ],
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [
      './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
      './storage/framework/views/*.php',
      './resources/views/**/*.blade.php',
  ],
  theme: {
      backgroundColor: theme => ({
          ...theme('colors'),
          'primary': 'rgba(252,219,60,0.92)',
          'primary-light': 'rgb(245,223,117)',


      }),
      borderColor: theme => ({
          ...theme('colors'),
          'primary': 'rgba(252,219,60,0.92)',
      }),
      textColor: theme => ({
          ...theme('colors'),
          'primary': 'rgba(252,219,60,0.92)',
      }),
      extend: {
          fontFamily: {
              malayalam: ['Malayalam', ...defaultTheme.fontFamily.sans],
          },
          fontSize: {
              'xs': '13px',
              'md': '16px'
          },
          colors: {

          },
          screens: {

          },
      },
  },
  variants: {},
  plugins: [],
}
