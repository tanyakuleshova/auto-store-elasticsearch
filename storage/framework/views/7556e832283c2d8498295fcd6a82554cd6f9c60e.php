<?php $__env->startSection('content'); ?>
    <input type="hidden" class="category_id" value="<?php echo e($category->id); ?>">
    <input type="hidden" class="category_link" value="<?php echo e(\Illuminate\Support\Str::slug($category->lang->name,'_')); ?>">
    <div class="">
        <?php echo $__env->make('product::themes.default.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="bg-gray-100 border-t-2 border-primary">
            <div class="container mx-auto py-4 px-2 md:px-8 max-w-screen-xl m-auto">
                
                <h1 class="text-center text-2xl my-6"><?php echo e($category->lang->name); ?></h1>
                <div class="md:flex justify-between flex-wrap md:flex-no-wrap">

                    <div class="w-64 pt-4 mx-auto plain filters_section">
                        <?php echo $__env->make('product::themes.default.partials.filters', ['all_filters'=>$all_filters], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                    <div class="w-full md:w-5/6 products_section relative">
                        <?php echo $__env->make('product::themes.default.partials.products', ['products'=>$products], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<script>
    window.attrs = [];

    document.addEventListener("DOMContentLoaded", function() {
        parse_filters(true);
        replace_pagination_buttons();
        ajax_pagination();
    });

    function choose_filter(_this){
        let atr_id = _this.dataset.atr_id;
        let atr_val = _this.dataset.atr_val;

        if(_this.checked){
            fill_atr_params(atr_id, atr_val);
        }else{
            remove_atr_params(atr_id, atr_val);
        }

        let url = get_url();
        history.pushState({}, null, url);

        apply_filters();
    }

    function apply_filters()
    {
        let id = document.querySelector('.category_id').value;
        let link = document.querySelector('.category_link').value;

        fetch("/category/"+id+"/"+link, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content

            },
            body: JSON.stringify({'filters':window.filters, 'ajax': true})
        })
            .then((response) => response.json())
            .then((response) => {
                if (response.success) {
                    update_page(response);
                    window.load_img();
                }
            })
    }

    function parse_filters(init=false){
        let filters = document.location.href.split('/filters-')[1];
        if(filters){
        filters = filters.split('?')[0];

            let arr = filters.split('-');
            arr.forEach(function (item){
                let atr = item.split('_');
                let checkbox = document.querySelector("input[data-atr_id='"+atr[0]+"'][data-atr_val='"+atr[1]+"']");
                if(checkbox) checkbox.checked = true;
                if(init){
                    fill_atr_params(atr[0], atr[1]);
                }
            });
        }
    }

    function ajax_pagination(){
        document.addEventListener('click',function(e){
            if(e.target && e.target.getAttribute('href')){
                if(e.target.getAttribute('href').includes('page')){
                    e.preventDefault();
                    let href = e.target.getAttribute('href');
                    if(href){
                        let page = href.split('page=')[1];
                        let url = get_url()+'?page='+page;
                        history.pushState({}, null, url);
                        fetch(url, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content

                            },
                            body: JSON.stringify({'ajax': true})
                        })
                            .then((response) => response.json())
                            .then((response) => {
                                if (response.success) {
                                    update_page(response);
                                    window.load_img();
                                }
                                replace_pagination_buttons();
                            })
                    }
                }
            }
        });
    }
    function update_page(response){
        let products_div = document.querySelector('.products_section');
        let filters_div = document.querySelector('.filters_section');
        products_div.innerHTML = response.view;
        filters_div.innerHTML = response.filters;
        parse_filters(false);
    }

    function replace_pagination_buttons()
    {
        let svg = document.querySelectorAll('.pagination svg.w-5');
        svg.forEach(function (item, i){
            if(i === 0){
                item.parentElement.innerHTML = "<";
            }else{
                item.parentElement.innerHTML = ">";
            }
        });
    }

    function fill_atr_params(atr_id, atr_val){
        window.attrs[atr_id] = window.attrs[atr_id] ? window.attrs[atr_id] : [];
        window.attrs[atr_id].push(atr_val);
        window.filters = [];

        window.attrs.forEach((item, key)=>{
            if(item.length){
                let f = {'atr_id': key, 'value_ids': item };
                window.filters.push(f);
            }
        });
    }

    function remove_atr_params(atr_id, atr_val){
        let arr = [];
        window.attrs[atr_id].forEach(function (item){
            if(item !== atr_val){
                arr.push(item);
            }
        });
        if(arr.length){
            window.attrs[atr_id] = arr;
        }else{
            delete window.attrs[atr_id];
        }

        window.filters = [];

        window.attrs.forEach((item, key)=>{
            if(item.length){
                let f = {'atr_id': key, 'value_ids': item };
                window.filters.push(f);
            }
        });
    }

    function get_url(){
        let str = '';
        let url = '';

        if(window.filters){
            window.filters.forEach((item)=>{
                let atr = item.atr_id;
                item.value_ids.forEach((item)=>{
                    str += '-' + atr +'_'+item;
                });
            });
            let id = document.querySelector('.category_id').value;
            let link = document.querySelector('.category_link').value;
            url = "/category" + "/" + id + "/" + link + '/filters' +str;
            if(!window.filters.length){
                url = "/category" + "/" + id + "/" + link;
            }
        }

        return url;
    }
</script>

<?php echo $__env->make('product::themes.default.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/tetiana/PhpstormProjects/whalebase/auto/app/Domains/Product/Resources/Views/themes/default/category.blade.php ENDPATH**/ ?>