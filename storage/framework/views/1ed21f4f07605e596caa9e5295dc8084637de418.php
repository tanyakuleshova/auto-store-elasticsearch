<div class="">
    <div class="md:flex flex-wrap  md:mx-4 md:justify-end justify-center">
        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($product->lang): ?>
                <div class="w-64 mx-auto xl:w-1/4 p-4 bg-white md:m-4 my-4 rounded flex flex-col justify-between">
                    <div>
                        <div class="text-center text-md mb-2 plain"><?php echo $product->lang->name; ?></div>
                        <div class="mx-auto" style="width: 200px">
                            <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="
                                 data-src="<?php echo $product->lang->link_rewrite; ?>" alt="">
                        </div>
                    </div>
                    <div class="text-xl font-bold mt-4"><?php echo $product->price; ?> <?php echo e(__('грн')); ?></div>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <div class="h-48">
        <?php if(count($products)): ?>
            <div class="py-2 px-3 pagination absolute lg:relative right-8 lg:right-auto" style="right: 1rem">
                <?php echo e($products->links('product::vendor.pagination.tailwind')); ?>

            </div>
        <?php endif; ?>
    </div>
</div>


<?php /**PATH /home/tetiana/PhpstormProjects/whalebase/auto/app/Domains/Product/Resources/Views/themes/default/partials/products.blade.php ENDPATH**/ ?>