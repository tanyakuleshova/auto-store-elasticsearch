<?php $__env->startSection('content'); ?>
    <div class="">
        <?php echo $__env->make('product::themes.default.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php echo \App\Domains\Product\Services\Outputting\HtmlOutputting::main_banner(); ?>

        <?php echo \App\Domains\Product\Services\Outputting\HtmlOutputting::main_menu(); ?>


    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('product::themes.default.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/tetiana/PhpstormProjects/whalebase/auto/app/Domains/Product/Resources/Views/themes/default/main.blade.php ENDPATH**/ ?>