<?php $__currentLoopData = $all_filters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $attribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <h1 class="text-lg py-2"><?php echo $attribute['key']; ?></h1>
        <div class="my-4" style="max-height: 250px; overflow-y: auto">

                <?php $__currentLoopData = $attribute['values']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="text-md">
                        <input onchange="choose_filter(this)" data-atr_id="<?php echo e($attribute['atr_id']); ?>" data-atr_val="<?php echo e($k); ?>" style="accent-color: #fce263 !important;" class="filter accent-green-500 w-4 h-4 text-primary focus:ring-0 focus:ring-offset-0 bg-gray-100 rounded border-gray-100 my-2" type="checkbox">
                        <span class="ml-2"><?php echo $value['name'] ?? ''; ?> (<?php echo e($value['doc_count'] ?? ''); ?>)</span>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php /**PATH /home/tetiana/PhpstormProjects/whalebase/auto/app/Domains/Product/Resources/Views/themes/default/partials/filters.blade.php ENDPATH**/ ?>