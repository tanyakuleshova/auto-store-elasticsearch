<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta name="robots" content="noindex"/>
    <meta name="googlebot" content="noindex"/>
    <title>Store</title>
    <link rel="stylesheet" href="<?php echo e(mix('css/app.css')); ?>">
    <?php echo $__env->yieldPushContent('styles'); ?>
</head>
<body class="font-malayalam">
<style>
    ::-webkit-scrollbar {
        width: 8px;
        border-radius: 5px;
        background: lightgray;
    }

    ::-webkit-scrollbar-thumb {
        border-radius: 3px;
        background: rgba(255, 227, 89, 0.92);
    }

</style>
<?php echo $__env->yieldContent('content'); ?>
<?php echo $__env->yieldPushContent('scripts'); ?>
<script src="<?php echo e(mix('js/app.js')); ?>" defer></script>

<div class="p-4 bg-gray-100 plain text-sm  ">
    <div class="text-left max-w-screen-xl m-auto">
        © 2022 the-extreme.shop. All rights reserved.
    </div>
</div>
</body>
</html>
<?php /**PATH /home/tetiana/PhpstormProjects/whalebase/auto/app/Domains/Product/Resources/Views/themes/default/layout.blade.php ENDPATH**/ ?>