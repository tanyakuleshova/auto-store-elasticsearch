<?php

namespace App\Support\Repository;

use Carbon\Carbon;

abstract class SQLAbstractRepository implements RepositoryInterface
{
    const PAGINATION = 10;

    public array $relations = [

    ];

    public function getAll()
    {
        return static::MODEL::orderBy('id', 'desc')->with($this->relations)->get();
    }

    public function getAllByParams($params)
    {
        return static::MODEL::where($params)->get();
    }
    public function getAllPaginated($order='desc')
    {
        return static::MODEL::orderBy('id', $order)->with($this->relations)->paginate(static::PAGINATION);
    }
    public function getPaginatedByParams($params, $order='desc')
    {
        return static::MODEL::where($params)->orderBy('id', $order)->with($this->relations)->paginate(static::PAGINATION);
    }
    public function firstByParams($params)
    {
        return static::MODEL::where($params)->with($this->relations)->first();
    }
    public function firstByParamsWithoutRelations($params)
    {
        return static::MODEL::where($params)->first();
    }
    public function firstBySearchParams($searchParams)
    {
        return static::MODEL::where((array)$searchParams)->first();
    }
    public function firstByParamsWithSpecifiedRelations($params, $with)
    {
        return static::MODEL::where($params)->with($with)->first();
    }

    public function limitByParams($params, $limit, $with='all', $order='desc')
    {
        $with = ($with == 'all') ? $this->relations : $with;
        return static::MODEL::where($params)->with($with)->orderBy('id', $order)->limit($limit)->get();
    }

    public function getSortedPaginated($field)
    {
        return static::MODEL::orderBy($field, 'asc')->with($this->relations)->paginate(static::PAGINATION);
    }

    public function getById($id)
    {
        return static::MODEL::where('id', $id)->with($this->relations)->first();
    }

    public function delete($id)
    {
        return static::MODEL::where('id', $id)->delete();
    }
    public function softDelete($id)
    {
        $item = static::MODEL::where('id',$id)->first();
        $item->active = 0;
        $item->deleted_at = Carbon::now();
        $item->save();
        return $item;
    }

    public function deleteByParams($params){
        return static::MODEL::where($params)->delete();
    }
    public function deleteByIds($ids){
        if(!empty($ids)){
            return static::MODEL::whereIn('id', $ids)->delete();
        }
        return 0;
    }
}
