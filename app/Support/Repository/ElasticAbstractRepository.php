<?php

namespace App\Support\Repository;

use App\Support\Service\DataTransformer\ElasticDataTransformer;
use Elastic\Elasticsearch\ClientBuilder;

abstract class ElasticAbstractRepository implements RepositoryInterface
{

    protected $elasticsearch;
    protected $transformer;

    public function __construct()
    {
        $this->elasticsearch = ClientBuilder::create()->setHosts([env('ELASTICSEARCH_HOST')])->build();
        $this->transformer = new ElasticDataTransformer();
    }

    public function toPaginated($hits): \Illuminate\Pagination\LengthAwarePaginator
    {
        return $this->transformer->transformToPaginatedCollection($hits);
    }

    public function getByParams($searchParams)
    {

    }
    public function getAll()
    {

    }

    public function getById($id)
    {

    }

    public function delete($id)
    {

    }
}
