<?php
namespace App\Support\Repository;

class AbstractSearchParams
{
    public function setParameter($k, $param)
    {
        if($k == 'with'){
            $array = explode(',', $param);
            $param = [];
            foreach ($array as $item){
                $param[] = trim($item);
            }
            if(count($param) > 1){
                $this->$k = $param;
            }elseif(count($param) == 1){
                if($param[0] !== ""){
                    $this->$k = $param;
                }
            }

        }else{
            $this->$k = $param;
        }
        return $this->$k;
    }
    public function getParameters(): static
    {
        return $this;
    }
}
