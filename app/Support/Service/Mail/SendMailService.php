<?php

namespace App\Support\Service\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailService extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $view;
    public $attachment;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $view)
    {
        $this->data = $data;
        $this->view = $view;
        $this->subject($data['subject'] ?? '');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        if(isset($this->data['attachment'])){
            return $this->view($this->view)->attach($this->data['attachment']);
        }
        return $this->view($this->view);

    }
}
