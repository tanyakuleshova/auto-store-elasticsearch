<?php
namespace App\Support\Service\DataTransformer;

use App\Domains\Properties\Models\OpenHome;
use App\Domains\Properties\Services\RedisService;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;

class ElasticDataTransformer
{
    public const PAGINATION = 9;
    public function transformToPaginatedCollection($hits): LengthAwarePaginator
    {
        $sources = Arr::pluck($hits, '_source');
        $newCollection = collect($sources);
        $page = request()->has('page') ? request('page') : 1;
        $perPage = self::PAGINATION;
        $offset = ($page * $perPage) - $perPage;
        $values = $newCollection->slice($offset, $perPage)->values();

        $values =  $this->addInFavouriteFlag($values);
        $values =  $this->addOpenHome($values);
        return  new LengthAwarePaginator(
            $values,
            $newCollection->count(),
            $perPage,
            $page,
            ['path' => request()->url(), 'query' => request()->query()]
        );
    }

    public function addInFavouriteFlag($values){
        if(request()->userObj){

            $favourites = RedisService::getFavourite(request()->userObj->id);

            $favouriteIDs = [];

            foreach ($favourites as $fav){
                $favouriteIDs[] = $fav->property_id;
            }
            $values = $values->map(function ($object) use ($favouriteIDs) {
                $object['in_favourites'] = in_array($object['id'], $favouriteIDs);
                return $object;
            });
        }
        return $values;
    }

    public function addOpenHome($values){
        $property_ids = [];
        foreach ($values as $value){
            $property_ids[] = $value['id'];
        }
        $now = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now())->setTimezone('Asia/Dubai')->format('Y-m-d H:i');
        $openHome = OpenHome::where('deleted', '!=', 1)->whereIn('property_id', $property_ids)->where('end', '>=', $now)->get();
        $values = $values->map(function ($object) use ($openHome) {
            foreach ($openHome as $event){
                if($event->property_id == $object['id']){
                    $object['open_home_id'] = $event->id;
                    $object['open_home_start'] = $event->start;
                }
            }
            return $object;
        });
        return $values;
    }
}
