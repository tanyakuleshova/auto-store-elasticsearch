<?php
namespace App\Support\Service\Traits;

trait WithTrait
{
    public function with($with){

        $array = $with ? explode(',', $with) : [];

        $param = [];
        foreach ($array as $item){
            $param[] = trim($item);
        }

        return $param;

    }
}
