<?php

namespace App\Support\Service\Validation;

interface ValidationInterface
{
    public function rules(): array;
}
