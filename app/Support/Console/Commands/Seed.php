<?php

namespace App\Support\Console\Commands;


use Database\Seeders\DatabaseSeeder;
use DirectoryIterator;
use Illuminate\Console\Command;

class Seed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:seed {--domain=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(DatabaseSeeder $seeder)
    {
        $domain = $this->option('domain') ?? '';

//        try {
            if ($domain) {
                $this->info('Seeding...');
                $class = "App\\Domains\\$domain\\Database\\Seeders\\DatabaseSeeder";
                if(class_exists($class)){
                    $seeder->run($class);
                    $this->info($domain);
                }
            } else {
                $domains = [];
                foreach (new DirectoryIterator('app/Domains') as $item) {
                    if ($item->isDot()) continue;
                    if ($item->isDir()) {
                        $domains[] = basename($item);
                    }

                };
                $this->info('Seeding...');
                foreach ($domains as $domain) {
                    $class = "App\\Domains\\$domain\\Database\\Seeders\\DatabaseSeeder";
                    if(class_exists($class)){
                        $seeder->run($class);
                        $this->info($domain);
                    }
                }
            }
//        } catch (\Exception $e) {
//            $this->info('Error occurred: ' . $e->getMessage());
//        }

        $this->info('Seeding completed');

    }

}
