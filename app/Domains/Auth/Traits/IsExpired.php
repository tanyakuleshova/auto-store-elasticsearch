<?php
namespace App\Domains\Auth\Traits;

use Carbon\Carbon;

trait IsExpired
{
    public function is_active($obj, $lifeTime): bool
    {
        $created_at = Carbon::createFromFormat('Y-m-d H:i:s', $obj->created_at);
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $diff_in_sec =  $created_at->diffInSeconds($now);
        if($diff_in_sec > $lifeTime){
            return false;
        }
        return true;
    }
}
