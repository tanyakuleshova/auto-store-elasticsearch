<?php

namespace App\Domains\Auth\Services\RecoveryPassword;

use App\Domains\Auth\Jobs\SendEmailResetLink;
use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Auth\Services\Validation\PasswordResetValidation;
use App\Domains\Auth\Traits\IsExpired;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use App\Support\Service\Mail\SendMailService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RecoveryPassword
{
    use IsExpired;
    const LIFETIME = 3600;

    public $userRepository;
    public $entity;
    public $service;
    public $validation;

    public function __construct(){
        $this->userRepository = new ProductRepository();
        $this->entity = new User();
        $this->service = new CanUserLogin();
        $this->validation = new PasswordResetValidation();
    }

    public function process($request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->validation->rules());

        $passwordReset = DB::table('password_resets')->where('token', $request->token)->first();
        if(!$passwordReset){
            return response()->json(['success'=>false, 'error'=>'token_not_found', 'message'=>'The provided token was not found.'], 422);
        }
        if($passwordReset->token == $request->token){
            if($this->is_active($passwordReset, self::LIFETIME)){
                $user = $this->userRepository->firstByParams(['email'=>$passwordReset->email]);
                $this->entity::store(['id'=> $user->id, 'password'=>$request->password]);
//                DB::table('password_resets')->where('email', $passwordReset->email)->delete();
                return response()->json(['success'=>true]);
            }
            return response()->json(['success'=>false, 'error'=>'token_expired', 'message'=>'The provided token was expired.'], 422);
        }
        return response()->json(['success'=>false, 'error'=>'wrong_token', 'message'=>'The provided token is incorrect.'], 422);
    }

    public function send($request)
    {
        $request->validate(['email'=>'required']);

        $user = $this->userRepository->firstByParams(['email'=>$request->email]);
        $status = $this->service->check($user);
        if(!$status['success']) return response()->json(['success'=>false, 'error'=>$status['error'], 'message'=>$status['message']], $status['status']);

        $passwordReset = $this->createPasswordReset($request);

        if($request->test){
            $link = (env('FRONT_URL') ?? '') . '/reset-password?token=' . $passwordReset->token;
            Mail::mailer('mailhog')->to($passwordReset->email)
                ->send(new SendMailService(
                        ['link'=>$link, 'subject'=>'Reset password'],
                        "auth::email_password_reset")
                );
        }else{
            SendEmailResetLink::dispatch($passwordReset);
        }


        return response()->json(['success'=>true]);
    }

    public function createPasswordReset($request)
    {
        $token = md5(uniqid());
        DB::table('password_resets')->where('email', $request->email)->delete();
        DB::table('password_resets')->insert([
            ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()],
        ]);
        return DB::table('password_resets')->where('email', $request->email)->first();
    }


}
