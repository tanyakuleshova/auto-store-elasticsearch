<?php

namespace App\Domains\Auth\Services\SocialAuth;

use Laravel\Socialite\Facades\Socialite;

class SocialProvider{

    protected $driver;

    public function __construct($driver)
    {
       $this->driver = $driver;
    }

    public function data(): \Laravel\Socialite\Contracts\User
    {
        return Socialite::driver($this->driver)->user();
    }

    public function redirect(): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        return Socialite::driver($this->driver)->redirect();
    }

}
