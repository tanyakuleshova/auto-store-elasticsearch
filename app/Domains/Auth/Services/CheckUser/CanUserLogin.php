<?php
namespace App\Domains\Auth\Services\CheckUser;

use App\Domains\Users\Repository\SQLRepository\ProductRepository;

class CanUserLogin
{
    public function check($user): array
    {
        if(!$user) return ['success'=> false,'error' => 'user_not_found', 'message'=> 'User not found.', 'status'=>401];

        if(!$user->active) return ['success'=> false, 'error' => 'user_is_not_active', 'message'=> 'The account is not active.', 'status'=>422];

        if(!$user->email_verified_at) return ['success'=> false,'error' => 'email_not_verified', 'message'=> 'Email not verified.', 'status'=>422];

        return ['success'=> true];
    }
}
