<?php

namespace App\Domains\Auth\Services\Verification;

use App\Domains\Auth\Models\PhoneVerification;
use App\Domains\Auth\Repository\SQLRepository\PhoneVerificationRepository;
use App\Domains\Notifications\Services\NotificationProviders\Slack;
use App\Domains\SmsProvider\Services\SendSms\SendSMSTwilio;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use Carbon\Carbon;

class ProcessPhoneVerification extends ProcessVerification
{

    public function __construct(
        protected PhoneVerificationRepository $repository,
        protected ProductRepository           $userRepository,
        protected SendSMSTwilio               $smsService
    ){}

    public function send($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'phone'=> User::PHONE_VALIDATION(),
            'country_code'=>'required'
        ]);

        $country_code = User::sanitizePhoneCountryCode($request->country_code);
        $phone = User::sanitizePhoneNumber($request->phone);
        $phone_number = $country_code.$phone;
        $verification = $this->createVerification($phone_number);

        if($request->test || $phone_number === "+971888888888"){
            Slack::send($verification->code);
        }else{
            $this->smsService::sendWithQueue($verification->phone, $verification->code);
        }
        return response()->json(['success'=>true]);
    }

    public function process($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'code'=>'required',
            'country_code'=>'required',
            'phone'=> User::PHONE_VALIDATION(),
        ]);

        $country_code = User::sanitizePhoneCountryCode($request->country_code);
        $phone = User::sanitizePhoneNumber($request->phone);
        $phone_number = $country_code.$phone;

        $verification = $this->repository->firstByParams(['phone'=>$phone_number]);
        $verifyStatus = $this->verify($request->code, $verification);

        if(!$verifyStatus['success']){
            return response()->json($verifyStatus, 422);
        }

        $users = $this->userRepository->getAllByParams(['phone'=>$phone, 'code'=>$country_code]);

        foreach ($users as $user){
            $user->phone_verified_at = Carbon::now();
            $user->save();
        }

        return response()->json(['success' => true]);
    }

    public function createVerification($phone)
    {
        $code = $this->generateCode();
        $this->repository->deleteByParams(['phone'=>$phone]);
        return PhoneVerification::store(['code'=>$code, 'phone'=>$phone]);
    }
}
