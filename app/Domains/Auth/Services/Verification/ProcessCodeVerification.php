<?php

namespace App\Domains\Auth\Services\Verification;

use App\Domains\Auth\Jobs\SendEmailResetCode;
use App\Domains\Auth\Jobs\SendEmailVerification;
use App\Domains\Auth\Repository\SQLRepository\CodeVerificationRepository;
use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use App\Support\Service\Mail\SendMailService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class ProcessCodeVerification extends ProcessVerification
{
    public function __construct(
        protected ProductRepository          $userRepository,
        protected User                       $entity,
        protected CanUserLogin               $service,
        protected CodeVerificationRepository $codeVerificationRepository
    ){}
    public function send($request): \Illuminate\Http\JsonResponse
    {
        $request->validate(['email'=>'required']);

        $user = $this->userRepository->firstByParams(['email'=>$request->email]);
        $status = $this->service->check($user);
        if(!$status['success']) return response()->json(['success'=>false, 'error'=>$status['error'] ?? '', 'message' => $status['message'] ?? ''], $status['status'] ?? 422);

        $resetCode = $this->createPasswordResetCode($request);

        if($request->test){
            Mail::mailer('mailhog')->to($resetCode->email)
                ->send(new SendMailService(
                        ['code'=>$resetCode->code, 'subject'=>'Reset password'],
                        "auth::email_password_code")
                );
        }else{
            SendEmailResetCode::dispatch($resetCode);
        }
        return response()->json(['success'=>true]);
    }

    public function process($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'code'=>'required',
            'email'=>'required|string|email|max:255',
            'password'=> ['required', 'string', 'regex:' . User::PASSWORD_REGEX, 'min:8', 'max:255']
        ]);

        $verification = $this->codeVerificationRepository->firstByParams(['email'=>$request->email]);
        $verifyStatus = $this->verify($request->code, $verification);

        if(!$verifyStatus['success']){
            return response()->json($verifyStatus, 422);
        }

        $user = $this->userRepository->firstByParams(['email'=>$request->email]);
        if(!$user) return response()->json(['success'=>false, 'error'=>'user_not_found', 'message'=>'No user found with this email address.'], 422);
        $user->password = $request->password;
        $user->save();
        DB::table('password_reset_codes')->where('email', $request->email)->delete();
        return response()->json(['success'=>true]);
    }

    public function createPasswordResetCode($request)
    {
        $code = $this->generateCode();
        DB::table('password_reset_codes')->where('email', $request->email)->delete();
        DB::table('password_reset_codes')->insert([
            ['email' => $request->email, 'code' => $code, 'created_at' => Carbon::now()],
        ]);
        return DB::table('password_reset_codes')->where('email', $request->email)->first();
    }
}
