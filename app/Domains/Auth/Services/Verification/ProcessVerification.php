<?php

namespace App\Domains\Auth\Services\Verification;

use App\Domains\Auth\Traits\IsExpired;


abstract class ProcessVerification
{
    const DIGITS = 4;
    const EXPIRATION = 3600;
    const ATTEMPTS = 2;

    use IsExpired;

    public function generateCode(): string
    {
        return str_pad(rand(0, pow(10, self::DIGITS)-1), self::DIGITS, '0', STR_PAD_LEFT);
    }

    public function verify($code, $verification): array
    {
        if($verification){
            if($verification->attempts < self::ATTEMPTS) {
                if ($verification->code == $code) {
                    if ($this->is_active($verification, self::EXPIRATION)) {
                        return ['success'=>true];
                    }
                    return ['success' => false, 'error' => 'code_expired', 'message'=>'The code has expired.'];
                }
                $verification->attempts += 1;
                $verification->save();
                return ['success' => false, 'error' => 'wrong_code', 'message'=>'The code is invalid.'];
            }
            return ['success' => false, 'error' => 'too_many_attempts', 'message'=>'Too many attempts. Try later.'];
        }else{
            return ['success'=>false, 'error'=>'code_was_not_sent',
                'message'=>'You are trying to verify an email, before verification code was requested.'];
        }
    }
}
