<?php

namespace App\Domains\Auth\Services\Verification;

use App\Domains\Auth\Jobs\SendEmailVerification;
use App\Domains\Auth\Models\EmailVerification;
use App\Domains\Auth\Repository\SQLRepository\EmailVerificationRepository;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use App\Support\Service\Mail\SendMailService;
use Carbon\Carbon;
use Http\Client\Exception;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;


class ProcessEmailVerification extends ProcessVerification
{
    public $repository;
    public $userRepository;
    public function __construct(){
        $this->repository = new EmailVerificationRepository();
        $this->userRepository = new ProductRepository();
    }
    protected const JWT_LIFETIME = 43200; // 30 days

    public function send($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'email'=>'required|string|email|max:255'
        ]);

        $verification = $this->createVerification($request->email);

        if($request->test){
            Mail::mailer('mailhog')->to($request->email)
                ->send(new SendMailService(
                        ['code'=>$verification->code, 'subject'=>'Email verification'],
                        "auth::email_verification")
                );
        }else{
            SendEmailVerification::dispatch($verification);
        }

        return response()->json(['success'=>true]);
    }

    public function process($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'code'=>'required',
            'email'=>'required|string|email|max:255'
        ]);

        $verification = $this->repository->firstByParams(['email'=>$request->email]);
        $verifyStatus = $this->verify($request->code, $verification);

        if(!$verifyStatus['success']){
            return response()->json($verifyStatus, 422);
        }

        //if user are logged in and wants to change email, in this case find this user by id and change email
        if($verification->user_id && !$request->registration){
            $user = $this->userRepository->firstByParamsWithoutRelations(['id'=>$verification->user_id]);
            if($user){
                $user->email = $verification->email;
            }
        // if user are not logged in and just make a first verification, find this user by email
        }else{
            $user = $this->userRepository->firstByParamsWithoutRelations(['email'=>$request->email]);
        }

        if($user){
            $user->email_verified_at = Carbon::now();
            $user->active = 1;
            try{
                $user->save();
            }catch (\Exception $e){
                return response()->json(['success' => false, 'error'=>'please_logout_correctly', 'message'=>'If you are on registration stage, it seems you are not logout correctly.']);
            }

            $user = $this->userRepository->firstByParamsWithoutRelations(['id'=>$user->id]);
            JWTAuth::factory()->setTTL(self::JWT_LIFETIME);
            $token = auth('api')->login($user);
            return response()->json(['success' => true, 'user'=>$user, 'token'=>$token, 'expired_at'=> date('d M Y h:i', auth('api')->payload()->toArray()['exp']) . " UTC"]);
        }

        return response()->json(['success' => false, 'error'=>'user_not_found'], 422);
    }

    public function createVerification($email)
    {
        $code = $this->generateCode();
        $this->repository->deleteByParams(['email'=>$email]);
        return EmailVerification::store(['code'=>$code, 'email'=>$email, 'user_id'=>auth('api')->id()]);
    }
}
