<?php

namespace App\Domains\Auth\Services\Validation;

use App\Domains\Users\Models\User;
use App\Support\Service\Validation\ValidationInterface;

class RegisterValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'code' => ['required', 'max:5'],
            'phone' => User::PHONE_VALIDATION(),
            'email'    => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'regex:' . User::PASSWORD_REGEX, 'min:8', 'max:255'],
        ];
    }
}
