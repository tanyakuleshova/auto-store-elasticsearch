<?php

namespace App\Domains\Auth\Services\Validation;

use App\Support\Service\Validation\ValidationInterface;

class LoginValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
            'email'    => 'required|string|email|max:255',
            'password' => ['required'],
            'remember_me'=>['nullable', 'bool']
        ];
    }
}
