<?php

namespace App\Domains\Auth\Services\Validation;

use App\Domains\Users\Models\User;
use App\Support\Service\Validation\ValidationInterface;

class PasswordResetValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
            'token' => ['string', 'required'],
            'password'=>['required', 'confirmed', 'string', 'regex:' . User::PASSWORD_REGEX, 'min:8', 'max:12'],
        ];
    }
}
