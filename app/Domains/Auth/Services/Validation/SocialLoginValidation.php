<?php

namespace App\Domains\Auth\Services\Validation;

use App\Support\Service\Validation\ValidationInterface;

class SocialLoginValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
           'facebook_id'=>'required'
        ];
    }
}
