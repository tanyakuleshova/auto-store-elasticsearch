<?php

namespace App\Domains\Auth\Services\Validation;

use App\Domains\Users\Models\User;
use App\Support\Service\Validation\ValidationInterface;

class SocialRegisterValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
            'facebook_id' => ['required_without:twitter_id', 'string'],
            'twitter_id' => ['required_without:facebook_id', 'string'],
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone' => User::PHONE_VALIDATION(),
            'email' => 'required|string|email|max:255',
            'email_verified_at' => ['nullable', 'date', 'date_format:Y-m-d'],
            'photo_url' => ['nullable', 'string'],
            'password' => ['required', 'string', 'regex:' . User::PASSWORD_REGEX, 'min:8', 'max:255'],
        ];
    }
}
