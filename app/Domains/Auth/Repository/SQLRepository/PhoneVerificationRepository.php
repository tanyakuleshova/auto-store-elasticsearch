<?php

namespace  App\Domains\Auth\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class PhoneVerificationRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Auth\Models\PhoneVerification';

    public array $relations = [

    ];

    public array $search_fields = [

    ];
}
