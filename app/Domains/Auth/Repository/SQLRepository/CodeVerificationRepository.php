<?php

namespace  App\Domains\Auth\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class CodeVerificationRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Auth\Models\CodeVerification';

    public array $relations = [

    ];

    public array $search_fields = [

    ];
}
