<?php

namespace  App\Domains\Auth\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class PasswordResetRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Auth\Models\PasswordReset';

    public array $relations = [

    ];

    public array $search_fields = [

    ];
}
