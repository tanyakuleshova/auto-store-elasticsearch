<?php

use App\Domains\Auth\Http\Controllers\AdminLoginController;
use App\Domains\Auth\Http\Controllers\AdminResetPasswordController;
use App\Domains\Auth\Http\Controllers\LoginController;
use App\Domains\Auth\Http\Controllers\RegisterController;
use App\Domains\Auth\Http\Controllers\ResetPasswordController;
use App\Domains\Auth\Http\Controllers\SocialAuthController;
use App\Domains\Auth\Http\Controllers\VerificationController;

//$this->router->group(['middleware' => 'request_handler', 'prefix' => 'auth'], function ($router) {
//    $router->post('/register', [RegisterController::class, 'register']); // +
//    $router->post('/login', [LoginController::class, 'login']); // +
//    $router->post('/logout', [LoginController::class, 'logout'])->middleware(['jwt.verify']); // +
//    $router->get('/social', [SocialAuthController::class, 'social']);
//    $router->post('/login_social', [SocialAuthController::class, 'login']);
//    $router->post('/register_social', [SocialAuthController::class, 'register']);
//    $router->get('/twitter_callback', [SocialAuthController::class, 'twitter_callback']);
//
//
//    $router->group(['prefix' => 'verification'], function ($router) { //+
//        $router->post('/email/send', [VerificationController::class, 'sendEmail'])->middleware(['ip_check', 'is_user_already_verified']);
//        $router->post('/email/verify', [VerificationController::class, 'verifyEmail'])->middleware(['is_user_already_verified']);
//        $router->post('/phone/send', [VerificationController::class, 'sendSms'])->middleware(['ip_check', 'is_user_already_verified']);
//        $router->post('/phone/verify', [VerificationController::class, 'verifyPhone'])->middleware(['is_user_already_verified']);
//    });
//
//    $router->group(['prefix' => 'reset'], function ($router) { // +
//        $router->post('/send_email_code', [ResetPasswordController::class, 'sendResetCode'])->middleware(['ip_check']);
//        $router->patch('/password', [ResetPasswordController::class, 'resetPassword']);
//    });
//});
//
//$this->router->group(['middleware' => 'request_handler', 'prefix' => 'admin'], function ($router) { //+
//
//    $this->router->group(['prefix' => 'auth'], function ($router) {
//        $router->post('/login', [AdminLoginController::class, 'login']);
//
//        $router->group(['prefix' => 'reset'], function ($router) {
//            $router->post('/send_email_link', [AdminResetPasswordController::class, 'sendResetLink']);
//            $router->patch('/password', [AdminResetPasswordController::class, 'resetPassword']);
//        });
//    });
//});



