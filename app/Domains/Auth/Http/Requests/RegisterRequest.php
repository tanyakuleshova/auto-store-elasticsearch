<?php

namespace App\Domains\Auth\Http\Requests;

use App\Domains\Users\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules() : array
    {
        return [
            'first_name' => 'string|max:255',
            'last_name' => 'string|max:255',
            'phone' => User::PHONE_VALIDATION(),
            'email'    => 'required|string|email|max:255|unique:users,email',
            'password' => ['required', 'confirmed', 'string', 'regex:' . User::PASSWORD_REGEX, 'min:8', 'max:12'],
        ];
    }
}
