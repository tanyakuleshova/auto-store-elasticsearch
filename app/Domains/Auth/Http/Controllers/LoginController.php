<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Auth\Services\Validation\LoginValidation;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use App\Support\Http\Controller as BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 *  @OA\SecurityScheme(
 *     type="http",
 *     description="Login with email and password to get the authentication token",
 *     name="Token based Based",
 *     in="header",
 *     scheme="bearer",
 *     bearerFormat="JWT",
 *     securityScheme="apiAuth",
 * ),
 * @OA\PathItem(
 *      path="/api/auth/login"
 *  ),
 * @OA\Post(path="/api/auth/login",
 *     tags={"auth"},
 *     summary="Route for login without socials.",
 *     description="
 *     This endpoint returns a user object and a jwt token.
 *     The token expires after 30 days. Use this token in the request headers (Only for secured routes. Secured routes has lock icon).
 *     For swagger save this token using Authorize button. And the token will be automatically added to the header on all secured routes.
 *     If the user passes invalid credentials, the response will be 'Unauthorized'",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"email", "password"},
 *        @OA\Property(property="email", type="string", example="test@test.com"),
 *        @OA\Property(property="password", type="string", example="Test1password")
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="token", type="bearer", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTI3LjAuMC4xOjgwMDAvYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE2NjE4NTk2MDAsImV4cCI6MTY2MTk0NjAwMCwibmJmIjoxNjYxODU5NjAwLCJqdGkiOiJ6T3V2TTk2RDF2SWszYTN4Iiwic3ViIjoiMzcyIiwicHJ2IjoiOGNhZGY2N2M0YjJkZmNmNDQzM2M2MThiNmQ4NzIyYzM0ZDJhYTMwMyJ9.2LgCA4rOrO5LKm3DNXL9n4b7lDaCBmHwVK3bmE2JP-w"),
 *        @OA\Property(property="user", type="obj", example={"id": 388,"first_name": "John","last_name": "Doe","email": "test@test.com","email_verified_at": "2022-09-07T00:00:00.000000Z","gender": "f","dob": "1993-08-01","phone": "+971527630345","phone_verified_at": null,"active": 1,"facebook_id": "1143095216272222","twitter_id": null,"deleted_at": null,
"address": {"id": 6,"user_id": 388,"country": "OAE12","city": "Dubai","street": "Highway str","zip_code": "3433236"},"media": {{"id": 107,"collection_name": "user_icon","file_name": "picture.jpeg","generated_conversions": {"md": true,"sm": true,"xs": true},
"order_column": 1,"original_url": "http://127.0.0.1:8000/media/07/107/picture.jpeg"}}}),
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="expired_at", type="string", example="01 Oct 2022 01:23 UTC"),
 *
 *    )
 * ),
 *     @OA\Response(response="401", description="Unauthorized",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="Unauthorized"),
 *        @OA\Property(property="message", type="string", example="Invalid password."))
 *     )
 * ),
 *  *  @OA\PathItem(
 *      path="/api/auth/logout"
 *  ),
 * @OA\Post(path="/api/auth/logout",
 *     tags={"auth"},
 *     summary="Route for logout.",
 *     description="
 *     This endpoint will add the jwt token to the black list. The user will be logged out.",
*       security={{ "apiAuth": {} }},
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *    )
 * ),
 *     @OA\Response(response="401", description="token expired or invalid",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_is_expired"),
 *    )
 *     )
 * )
 */
class LoginController extends BaseController
{
    public function __construct(
        protected ProductRepository $repository,
        protected User              $entity,
        protected LoginValidation   $validation,
        protected CanUserLogin      $service,

    ){}
    protected const JWT_LIFETIME = 43200; // 30 days

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->validation->rules());

        JWTAuth::factory()->setTTL(self::JWT_LIFETIME);

        $user = User::where('email', $request->email)->first();
        if(!$user){
            return response()->json(['success'=>false, 'error' => 'Unauthorized', 'message'=>'The provided email does not exist.'], 401);
        }
        if (!$request->token = auth('api')->attempt($request->only('email', 'password'), ['exp' =>Carbon::now()->addHour()->timestamp])) {
            return response()->json(['success'=>false, 'error' => 'Unauthorized', 'message'=>'Invalid password.'], 401);
        }
        $status = $this->service->check($user);
        if(!$status['success']) return response()->json(['success'=>false, 'error'=>$status['error'] ?? '', 'message'=>$status['message'] ?? ''], 422);

        return response()->json([
            'token' => $request->token,
            'user' => $user,
            'success'=>true,
            'expired_at'=> date('d M Y h:i', auth('api')->payload()->toArray()['exp']) . " UTC",
        ]);
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        auth('api')->logout();
        return response()->json(['success'=>true]);
    }
}
