<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Auth\Services\Validation\LoginValidation;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use App\Support\Http\Controller as BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 *
 * @OA\PathItem(
 *      path="/api/admin/auth/login"
 *  ),
 * @OA\Post(path="/api/admin/auth/login",
 *     tags={"admin auth"},
 *     summary="Login route for admins.",
 *     description="
 *     This endpoints if for login to admin panel. It returns user object and jwt token.
 *     Password and email required.
 *     Admin credentials are created in superadmin. But now for development purposes use /api/test/user/create.
 *     The token is returned in the response body. Use this token in the request headers (Only for secured routes. Secured routes in swagger has lock icon).
 *     For swagger save this token using Authorize button. And the token will be automatically added to the header on all secured routes.
 *     The remember_me parameter is optional.
 *     If remember_me is true, the token lifetime will be 7 days.
 *     By default, token lifetime is 1 day.
 *     No need to update token. If token is expired user should make login.",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"email", "password"},
 *        @OA\Property(property="email", type="string", example="admin@admin.com"),
 *        @OA\Property(property="password", type="string", example="AdminTest1"),
 *        @OA\Property(property="remember_me", type="bool", example=true),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="token", type="bearer", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTI3LjAuMC4xOjgwMDAvYXBpL2FkbWluL2F1dGgvbG9naW4iLCJpYXQiOjE2NjE4NjM5NzQsImV4cCI6MTY2MTk1MDM3NCwibmJmIjoxNjYxODYzOTc0LCJqdGkiOiJEMFBYMmlqN2s2TGRsWGhyIiwic3ViIjoiMzczIiwicHJ2IjoiOGNhZGY2N2M0YjJkZmNmNDQzM2M2MThiNmQ4NzIyYzM0ZDJhYTMwMyJ9.EKz-6JAkk3fpG4irwDx_D2fE2XdoK8yjoebnqLxr484"),
 *        @OA\Property(property="user", type="obj", example={ "id": 373, "first_name": "Jaclyn", "last_name": "Kellen", "email": "user@mail.com", "email_verified_at": "2022-08-30T12:52:19.000000Z", "gender": null, "dob": null, "phone": null, "phone_verified_at": null, "active": 1, "facebook_token": null, "twitter_token": null, "deleted_at": null}),
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="expired_at", type="string", example="07 Sep 2022 05:40 UTC"),
 *
 *    )
 * ),
 *     @OA\Response(response="422", description="validation error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example={"email": {"The email field is required."}}))
 *     )
 * )
 */
class AdminLoginController extends BaseController
{
    public function __construct(
        protected ProductRepository $repository,
        protected User              $entity,
        protected LoginValidation   $validation,
        protected CanUserLogin      $service,

    ){}

    protected const JWT_LIFETIME = 1440; // 1 day
    protected const JWT_LIFETIME_REMEMBER_BE = 10080; // 7 days

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->validation->rules());

        $lifetime = self::JWT_LIFETIME;
        if($request->remember_me){
            $lifetime = self::JWT_LIFETIME_REMEMBER_BE;
        }

        JWTAuth::factory()->setTTL($lifetime);

        if (!$request->token = auth('api')->attempt($request->only('email', 'password'))) {
            return response()->json(['success'=>false,'error' => 'Unauthorized', 'message'=>'The email or password is incorrect.'], 401);
        }
        $user = $this->repository->firstByParams(['email'=>$request->email]);

        if(!($user->isAdmin()  || $user->isSuper())){
            return response()->json(['success'=>false, 'error'=>'permission_denied', 'message'=>'Permission denied.'], 401);
        }
//        $user = auth('api')->user();
//        $user = $this->repository->firstByParams(['id'=>$user->id]);

        return response()->json([
            'token' => $request->token,
            'user' => $user,
            'expired_at'=> date('d M Y h:i', auth('api')->payload()->toArray()['exp']) . " UTC",
            'success'=>true
        ]);
    }
}
