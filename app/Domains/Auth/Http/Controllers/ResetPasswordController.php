<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\RecoveryPassword\RecoveryPassword;
use App\Domains\Auth\Services\Verification\ProcessCodeVerification;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;

/**
 * @OA\PathItem(
 *      path="/api/auth/reset/send_email_code"
 *  ),
 * @OA\Post(path="/api/auth/reset/send_email_code",
 *     tags={"auth"},
 *     summary="Route for sending an email with a password reset code",
 *     description="
 *     This endpoint sends a password reset code (4 digits) to the specified email.
 *     The limit of requests per hour is 10.",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"email"},
 *        @OA\Property(property="email", type="string", example="test@test.com"),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *    )
 * ),
 *     @OA\Response(response="401", description="Unauthorized",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="user_not_found")),
 *     )
 * ),
 *  *  @OA\PathItem(
 *      path="/api/auth/reset/password"
 *  ),
 * @OA\Patch(path="/api/auth/reset/password",
 *     tags={"auth"},
 *     summary="Route to reset password.",
 *     description="
 *     This endpoint resets password. Pass the code that was received by e-mail, email, and new password.
 *     The code is valid for 1 hour.
 *     The number of attempts to submit the code is 2.
 *     If the user submits an invalid code more than 2 times or the code has expired, you need to send him a new code using the /api/auth/reset/password/send_email_code route.",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"code", "password", "email"},
 *        @OA\Property(property="code", type="string", description="4 digits", example="3294"),
 *        @OA\Property(property="email", type="string", example="test@test.com"),
 *        @OA\Property(property="password", type="string", description="Allowed format: min 8 character, min 1 upper letter, min 1 lower letter, min 1 number", example="Test1password",  minLength=8),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *    )
 * ),
 *     @OA\Response(response="422", description=" error (wrong_code | code_expired | too_many_attempts | code_not_found)",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="code_expired"),
 *    )
 *     )
 * ),
 */
class ResetPasswordController extends BaseController
{
    public function __construct(
        protected ProcessCodeVerification $codeVerification
    ){}

    public function sendResetCode(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->codeVerification->send($request);
    }

    public function resetPassword(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->codeVerification->process($request);
    }
}
