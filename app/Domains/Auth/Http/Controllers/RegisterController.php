<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\Validation\RegisterValidation;
use App\Domains\Auth\Services\Verification\ProcessEmailVerification;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;


/**
 * @OA\OpenApi(
 *  @OA\Info(
 *      title="Seer API",
 *      version="1.0.0",
 *      description="API documentation for Seer
 *      Users with test data:
 *      ADMIN: email: seer@admin.com, password: NiceNice39 (POST /api/admin/auth/login),
 *      USER: email: user@seer.com, password: NiceNice39 (POST /api/auth/login)",
 *  ),
 *  @OA\Server(
 *      description="Seer API",
 *      url=L5_SWAGGER_CONST_HOST
 *  ),
 *  @OA\PathItem(
 *      path="/api/auth/register"
 *  ),
 *  @OA\Post(path="/api/auth/register",
 *     tags={"auth"},
 *     summary="Route for registering user without socials.",
 *     description="
 *     This endpoint returns a user object that has an unverified email and has active = false.
 *     The user can log in only after confirmation by e-mail.
 *     After email confirmation, the user will have active=true.
 *     In case account was previously deleted it will be restored, but email need to be verified.
 *     The next step in both cases is api/auth/verification/email/send.",
 *     operationId="createUser",
 *     @OA\RequestBody(
 *         required=true,
 *         description="Created user object",
 *      @OA\JsonContent(
 *        type="object",
 *        required={"first_name", "last_name", "phone", "email", "password"},
 *        @OA\Property(property="first_name", type="string", description="", example="Alex", maxLength=255),
 *        @OA\Property(property="last_name", type="string", description="", example="Brave",  maxLength=255),
 *        @OA\Property(property="code", type="string", example="+971", description="Allowed characters are:, +, numbers.", minLength=3, maxLength=4),
 *        @OA\Property(property="phone", type="string", example="527630345", description="Allowed characters are: dashes, +, numbers.", minLength=10, maxLength=18),
 *        @OA\Property(property="email", type="string", example="nice@gmail.com"),
 *        @OA\Property(property="password", type="string", description="Allowed format: min 8 character, min 1 upper letter, min 1 lower letter, min 1 number", example="NiceNice39",  minLength=8),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="obj", example={
 *     "id": 40, "first_name": "Alex", "last_name": "Brave", "email": "nice@gmail.com", "email_verified_at": null, "gender": null, "dob": null, "phone": "+971222555222", "fax": null, "phone_verified_at": null, "active": 0, "id_card_verified": null, "twitter_id": null,"facebook_id": null,"fcm_token": null,"stripe_id": null,"role": "User", "deleted_at": null, "created_at": "2023-01-06T11:14:33.000000Z", "updated_at": "2023-01-06T11:14:33.000000Z"
 *     })
 *    )
 * ),
 *     @OA\Response(response="422", description="validation error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example={"first_name": { "The first name field is required." }, "last_name": { "The last name field is required." }, "phone": { "The phone field is required." }, "email": { "The email field is required." }, "password": { "The password field is required."}})
 *    )
 *     )
 * ),

 * )
 */
class RegisterController extends BaseController
{
    public function __construct(
        protected ProductRepository        $repository,
        protected User                     $entity,
        protected RegisterValidation       $validation,
        protected ProcessEmailVerification $emailVerification
    ){}

    public function register(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->validate($this->validation->rules());
        $user = $this->repository->firstByParams(['email'=>$request->email]);
        if($user){
            if(!$user->active){
                $data['id'] = $user->id;
                $data['active'] = true;
                $user = $this->entity::store($data);
            }else{
                return response()->json(['success' => false, 'error'=>'email_already_exist'], 422);
            }
        }else{
            $data['role'] = User::ROLE_USER;
            $user = $this->entity::store($data);
            $user = $this->repository->firstByParamsWithoutRelations(['id'=>$user->id]);
        }

        return response()->json(['success' => true, 'user'=>$user]);
    }
}
