<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\RecoveryPassword\RecoveryPassword;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;

/**
 * @OA\PathItem(
 *      path="/api/admin/auth/reset/send_email_link"
 *  ),
 * @OA\Post(path="/api/admin/auth/reset/send_email_link",
 *     tags={"admin auth"},
 *     summary="Route for send reset password link.",
 *     description="An email with a link will be sent to the user. By clicking on the link, the user will see a page with a password reset form. Link Example: https://domain/reset/password?token=07ec7ca049bf517a60140f661f61f5bc. The token parameter from this link will be used in the password reset route.",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"email"},
 *        @OA\Property(property="email", type="string", example="user@mail.com"),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *    )
 * ),
 *
 *     @OA\Response(response="422", description="validation error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example={"email": {"The email field is required."}}))
 *     )
 * ),
 *  *  @OA\PathItem(
 *      path="/api/admin/auth/reset/password"
 *  ),
 * @OA\Patch(path="/api/admin/auth/reset/password",
 *     tags={"admin auth"},
 *     summary="Route for reset password.",
 *     description="The token is obtained from the link that the user received by email.",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"token", "password", "password_confirmation"},
 *        @OA\Property(property="token", type="string", example="07ec7ca049bf517a60140f661f61f5bc"),
 *        @OA\Property(property="password", type="alphanumeric string with spec chars", example="rrr$123456",  minLength=8, maxLength=12),
 *        @OA\Property(property="password_confirmation", type="string", example="rrr$123456")
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *    )
 * ),
 *     @OA\Response(response="422", description=" error (invalid_token | token_expired)",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="invalid_token"),
 *    )
 *     )
 * ),
 */
class AdminResetPasswordController extends BaseController
{
    public function __construct(
        protected RecoveryPassword $recoveryPasswordService,
    ){}

    public function sendResetLink(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->recoveryPasswordService->send($request);
    }

    public function resetPassword(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->recoveryPasswordService->process($request);
    }
}
