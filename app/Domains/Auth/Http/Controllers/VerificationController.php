<?php

namespace App\Domains\Auth\Http\Controllers;

use App\Domains\Auth\Services\Verification\ProcessEmailVerification;
use App\Domains\Auth\Services\Verification\ProcessPhoneVerification;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;


/**
 * @OA\PathItem(
 *      path="/api/auth/verification/email/send"
 *  ),
 * @OA\Post(path="/api/auth/verification/email/send",
 *     tags={"auth"},
 *     summary="Route for sending email verification.",
 *     description="
 *     This endpoint sends an email with a code (4 digits) to the specified email address.
 *     Email confirmation required. The user will be inactive until an email confirmation is done.
 *     The limit of requests per hour is 10.
 *     The next step is api/auth/verification/email/verify.",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"email"},
 *        @OA\Property(property="email", type="string", example="test@test.com"),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *    )
 * ),
 *     @OA\Response(response="422", description="validation error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example={"email": {"The email field is required."}}))
 *     )
 * ),
 *   @OA\PathItem(
 *      path="/api/auth/verification/email/verify"
 *  ),
 * @OA\Post(path="/api/auth/verification/email/verify",
 *     tags={"auth"},
 *     summary="Route for email confirmation.",
 *     description="
 *     This endpoint performs email verification.
 *     If user verify email while registration send additional field: registration=true or simply do not send token.
 *     The user passes the code that was received by e-mail and an email.
 *     After successful verification, the user will be activated and logged in.
 *     The code is valid for 1 hour.
 *     The number of attempts to submit the code is 2.
 *     If the user submits an invalid code more than 2 times or the code has expired, you need to send him a new code using the /api/auth/verification/email/send route.",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"email", "code"},
 *        @OA\Property(property="email", type="string", example="test@test.com"),
 *        @OA\Property(property="code", type="string", example="2388"),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="obj", example={"id": 655,"first_name": "John","last_name": "Doe","email": "d@d","email_verified_at": "2022-09-09T10:13:03.000000Z","gender": null,"dob": null,"phone": "+971527630345","phone_verified_at": null,"active": 1,"facebook_id": null,"twitter_id": null,"fcm_token": null,"deleted_at": null,"address": null,"media": {}}),
 *        @OA\Property(property="token", type="string", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vc2Vlcl9iZS5sb2NhbC9hcGkvYXV0aC92ZXJpZmljYXRpb24vZW1haWwvdmVyaWZ5IiwiaWF0IjoxNjYyNzE4MzgzLCJleHAiOjE2NjI4MDQ3ODMsIm5iZiI6MTY2MjcxODM4MywianRpIjoid00xVHBqdWlWN0R2QW9yQSIsInN1YiI6IjY1NSIsInBydiI6IjhjYWRmNjdjNGIyZGZjZjQ0MzNjNjE4YjZkODcyMmMzNGQyYWEzMDMifQ.85EV9GjJtz-nlpkrEY4EdACvwWa1nMkrJzBGlGacm9I")
 *    )
 * ),
 *     @OA\Response(response="422", description="verification error (wrong_code | too_many_attempts | code_expired | code_not_found)",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="wrong_code"),
 *    )
 *     )
 * ),
 *  *   @OA\PathItem(
 *      path="/api/auth/verification/phone/send"
 *  ),
 * @OA\Post(path="/api/auth/verification/phone/send",
 *     tags={"auth"},
 *     summary="Route for sending SMS with a code to verify the phone number.",
 *     description="
 *     This endpoint sends the SMS with a code (4 digits) to the specified phone number.
 *     The limit of requests per hour is 10.
 *     For testing purposes, send the 'test=true' parameter or set the phone number to +971888888888 and the code will be sent to the slack channel.",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"phone"},
 *        @OA\Property(property="country_code", type="string", example="+971", description="Allowed characters are: dashes, +, numbers.", minLength=3),
 *        @OA\Property(property="phone", type="string", example="527630345", description="Allowed characters are: dashes, numbers.", minLength=7, maxLength=18),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *    )
 * ),
 *     @OA\Response(response="401", description="Unauthorized.",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="user_not_found"),
 *    )
 *     )
 * ),
 *  *   @OA\PathItem(
 *      path="/api/auth/verification/phone/verify"
 *  ),
 * @OA\Post(path="/api/auth/verification/phone/verify",
 *     tags={"auth"},
 *     summary="Route for phone number confirmation.",
 *     description="
 *     This endpoint performs phone verification.",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"phone", "code"},
 *        @OA\Property(property="country_code", type="string", example="+971", description="Allowed characters are: dashes, +, numbers.", minLength=3),
 *        @OA\Property(property="phone", type="string", example="527630345", description="Allowed characters are: dashes, +, numbers.", minLength=10, maxLength=18),
 *        @OA\Property(property="code", type="string", example="2388"),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *    )
 * ),
 *     @OA\Response(response="422", description="verification error (wrong_code | too_many_attempts | code_expired | code_not_found)",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="wrong_code"),
 *    )
 *     )
 * ),
 *  *   @OA\PathItem(
 *      path="/api/auth/verification/phone/send"
 *  ),
 */
class VerificationController extends BaseController
{
    public function __construct(
        protected ProcessEmailVerification $emailVerification,
        protected ProcessPhoneVerification $phoneVerification,
    ){}

    public function sendEmail(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->emailVerification->send($request);
    }

    public function verifyEmail(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->emailVerification->process($request);
    }

    public function sendSms(Request $request): \Illuminate\Http\JsonResponse
    {
       return $this->phoneVerification->send($request);
    }

    public function verifyPhone(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->phoneVerification->process($request);
    }

}
