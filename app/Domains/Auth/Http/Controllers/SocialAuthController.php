<?php

namespace App\Domains\Auth\Http\Controllers;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Domains\Auth\Services\CheckUser\CanUserLogin;
use App\Domains\Auth\Services\Validation\SocialLoginValidation;
use App\Domains\Auth\Services\Validation\SocialRegisterValidation;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use App\Support\Http\Controller as BaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 *   @OA\PathItem(
 *      path="/api/auth/login_social"
 *  ),
 * @OA\Post(path="/api/auth/login_social",
 *     tags={"auth"},
 *     summary="Route for login user with social media.",
 *     description="
 *     This endpoint is for user login via social media.
 *     Required parameters: facebook_id. If a user with the provided facebook_id exists, the user will be logged in.
 *     Notice that login via Twitter works differently.
 *     Documentation of Authentication via Twitter: https://docs.google.com/document/d/1RsfoL_2CzZs0zXnSSHZUHXLS2Q_y3qO0W7aZctuKVRc/edit?usp=sharing",
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"facebook_id"},
 *        @OA\Property(property="facebook_id", description="Specify facebook_id.", type="string", example="1143095216272222")
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="token", type="bearer", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTI3LjAuMC4xOjgwMDAvYXBpL2F1dGgvbG9naW5fc29jaWFsIiwiaWF0IjoxNjYxODYyNzg3LCJleHAiOjE2NjE5NDkxODcsIm5iZiI6MTY2MTg2Mjc4NywianRpIjoiSDFzQ3RsRm1XdUFuWHF3TCIsInN1YiI6IjMyNiIsInBydiI6IjhjYWRmNjdjNGIyZGZjZjQ0MzNjNjE4YjZkODcyMmMzNGQyYWEzMDMifQ.fb7kGUCCMeYKBENfVIVsFCowLuaqqgbBQXdKYxhjBys"),
 *        @OA\Property(property="user", type="obj", example={"id": 388,"first_name": "John","last_name": "Doe","email": "test@test.com","email_verified_at": "2022-09-07T00:00:00.000000Z","gender": "f","dob": "1993-08-01","phone": "+971527630345","phone_verified_at": null,"active": 1,"facebook_id": "1143095216272222","twitter_id": null,"deleted_at": null,
"address": {"id": 6,"user_id": 388,"country": "OAE12","city": "Dubai","street": "Highway str","zip_code": "3433236"},"media": {{"id": 107,"collection_name": "user_icon","file_name": "picture.jpeg","generated_conversions": {"md": true,"sm": true,"xs": true},
"order_column": 1,"original_url": "http://127.0.0.1:8000/media/07/107/picture.jpeg"}}}),
 *        @OA\Property(property="success", type="bool", example=true),
 *       @OA\Property(property="expired_at", type="string", example="01 Oct 2022 01:23 UTC")
 *    )
 * ),
 *     @OA\Response(response="422", description="user_not_found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="user_not_found"),
 *        @OA\Property(property="facebook_id", type="string", example="1143095216272222"))
 *     )
 * ),
*  @OA\PathItem(
 *      path="/api/auth/register_social"
    *  ),
 *  @OA\Post(path="/api/auth/register_social",
 *     tags={"auth"},
 *     summary="Route for user registration through social media.",
 *     description="
 *     This endpoint creates user and expects user data and twitter_id or facebook_id.
 *     If the email was provided by social media, send email_verified_at parameter.
 *     Image from photo_url will be upload if the user does not have a previously installed profile photo.
 *     Documentation of Authentication via Twitter: https://docs.google.com/document/d/1RsfoL_2CzZs0zXnSSHZUHXLS2Q_y3qO0W7aZctuKVRc/edit?usp=sharing",
 *     @OA\RequestBody(
 *         required=true,
 *         description="Created user object",
 *      @OA\JsonContent(
 *        type="object",
 *        required={"first_name", "last_name", "phone", "email", "password", "facebook_id"},
 *        @OA\Property(property="first_name", type="string", description="", example="Alex", maxLength=255),
 *        @OA\Property(property="last_name", type="string", description="", example="Brave",  maxLength=255),
 *        @OA\Property(property="phone", type="string", example="971527630345", description="Allowed characters are: dashes, +, numbers.", minLength=10, maxLength=18),
 *        @OA\Property(property="email", type="string", example="test@test.com"),
 *        @OA\Property(property="password", type="string", description="Allowed format: min 8 character, min 1 upper letter, min 1 lower letter, min 1 number", example="Test1password",  minLength=8),
 *        @OA\Property(property="facebook_id", type="string", example="1143095216272222"),
 *        @OA\Property(property="email_verified_at", type="string", example="2022-09-07", description="If the email is provided by social media, send this field. Allowed format:Y-m-d."),
 *        @OA\Property(property="photo_url", type="string", example="https://graph.facebook.com/v3.3/1143095216272222/picture?type=normal", description="The photo url from social media."),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="obj", example={"id": 388,"first_name": "John","last_name": "Doe","email": "test@test.com","email_verified_at": "2022-09-07T00:00:00.000000Z","gender": "f","dob": "1993-08-01","phone": "+971527630345","phone_verified_at": null,"active": 1,"facebook_id": "1143095216272222","twitter_id": null,"deleted_at": null,
"address": {"id": 6,"user_id": 388,"country": "OAE12","city": "Dubai","street": "Highway str","zip_code": "3433236"},"media": {{"id": 107,"collection_name": "user_icon","file_name": "picture.jpeg","generated_conversions": {"md": true,"sm": true,"xs": true},
"order_column": 1,"original_url": "http://127.0.0.1:8000/media/07/107/picture.jpeg"}}})
*    )
 * ),
 *     @OA\Response(response="422", description="validation error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example={"email": {"The email field is required."}})
*    )
 *     )
 * ),
 *
  */
class SocialAuthController extends BaseController
{
    public function __construct(
        protected ProductRepository        $repository,
        protected User                     $entity,
        protected CanUserLogin             $service,
        protected SocialLoginValidation    $loginValidation,
        protected SocialRegisterValidation $registerValidation,
    ){}
    protected const JWT_LIFETIME = 43200; // 30 days

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->loginValidation->rules());
        if($request->facebook_id){
            $user = $this->repository->firstByParams(['facebook_id'=>$request->facebook_id]);
            if(!$user){
                return response()->json(['success'=>false, 'error'=>'user_not_found', 'facebook_id'=>$request->facebook_id], 422);
            }
            $status = $this->service->check($user);
            if(!$status['success']) return response()->json(['success'=>false, 'error'=>$status['error']], 422);
            JWTAuth::factory()->setTTL(self::JWT_LIFETIME);
            $token = auth('api')->login($user);
            return response()->json(['success' => true, 'user'=>$user, 'token'=>$token, 'expired_at'=> date('d M Y h:i', auth('api')->payload()->toArray()['exp']) . " UTC"]);
        }
        return response()->json(['success' => false, 'error'=>'not_enough_data'], 422);
    }

    public function register(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->validate($this->registerValidation->rules());

        unset($data['photo_url']);
        $user = $this->repository->firstByParamsWithoutRelations(['email'=>$request->email]);

        if($user){
            $data['id'] = $user->id;
        }
        if($request->email_verified_at){
            $data['active'] = true;
        }
        $data['role'] = User::ROLE_USER;
        $entity = $this->entity::store($data);
        $user = $this->repository->firstByParamsWithoutRelations(['id'=>$entity->id]);
        if($request->photo_url){
            if($user->getMedia('user_icon')->isEmpty()){
                try{
                    $user->addMediaFromUrl($request->photo_url)->toMediaCollection('user_icon');
                }catch(\Exception $e){
                    return response()->json(['success' => false, 'error'=>$e->getMessage()], 500);
                }
            }
        }
        $user = $this->repository->firstByParamsWithoutRelations(['id'=>$entity->id]);
        if($user->active){
            JWTAuth::factory()->setTTL(self::JWT_LIFETIME);
            $token = auth('api')->login($user);
            return response()->json(['success' => true, 'user'=>$user, 'token'=>$token, 'expired_at'=> date('d M Y h:i', auth('api')->payload()->toArray()['exp']) . " UTC"]);
        }

        return response()->json(['success' => true, 'user'=>$user]);
    }

    public function twitter_callback(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->all();
        $connection = new TwitterOAuth(env('TWITTER_API_KEY'), env('TWITTER_API_SECRET'));

        $params = array("oauth_verifier" => $data['oauth_verifier'],"oauth_token"=>$data['oauth_token']);
        $access_token = $connection->oauth("oauth/access_token", $params);

        $connection = new TwitterOAuth(env('TWITTER_API_KEY'), env('TWITTER_API_SECRET'),
            $access_token['oauth_token'], $access_token['oauth_token_secret']);
        $content = $connection->get("account/verify_credentials");

        if($content->id){
            $user = $this->repository->firstByParams(['twitter_token'=>$content->id]);
            if(!$user){
                return response()->json(['success'=>false, 'error'=>'user_not_found', 'twitter_id'=>$content->id], 422);
            }
            JWTAuth::factory()->setTTL(self::JWT_LIFETIME);
            $token = auth('api')->login($user);
            return response()->json(['success' => true, 'user'=>$user, 'token'=>$token, 'expired_at'=> date('d M Y h:i', auth('api')->payload()->toArray()['exp']) . " UTC"]);
        }

        return response()->json(['success'=>false, 'error'=>'unexpected_error'], 422);
    }

    //it's for web version of sign in trough twitter (used for test). Something like this should be implemented in flutter
    public function social(Request $request)
    {
        if($request->get('provider') == 'twitter'){
            $connection = new TwitterOAuth(env('TWITTER_API_KEY'), env('TWITTER_API_SECRET'));
            $temporary_credentials = $connection->oauth('oauth/request_token', array("oauth_callback" =>env('TWITTER_REDIRECT')));
            $url = $connection->url("oauth/authorize", array("oauth_token" => $temporary_credentials['oauth_token']));
            return redirect($url);
        }
    }

}
