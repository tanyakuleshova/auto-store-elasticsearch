<?php

namespace App\Domains\Auth\Http\Middleware;

use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use Closure;

class IsUserAlreadyVerified
{
    public function handle($request, Closure $next): mixed
    {
        if($request->email){
            $user = (new ProductRepository)->firstByParamsWithoutRelations(['email'=>$request->email]);
            if($user){
                if($user->email_verified_at) return response()->json(['success'=>false, 'error'=>'email_already_verified'], 422);
            }else{
               //nothing, because if user are logged in and want to change email we will mot found user by email
            }
        }
        if($request->phone && $request->country_code){
            $country_code = User::sanitizePhoneCountryCode($request->country_code);
            $phone = User::sanitizePhoneNumber($request->phone);

            $user = (new ProductRepository)->firstByParamsWithoutRelations(['phone'=>$phone, 'code'=>$country_code]);
            if($user){
                if($user->phone_verified_at){
                    return response()->json(['success'=>false, 'error'=>'phone_already_verified'], 422);
                }
            }else{
                return response()->json(['success'=>false, 'error'=>'user_not_found'], 422);
            }
        }

        return $next($request);
    }
}
