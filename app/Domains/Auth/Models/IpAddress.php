<?php

namespace App\Domains\Auth\Models;
use Illuminate\Database\Eloquent\Model;


class IpAddress extends Model
{
    public $table = 'ip_addresses';
    public $guarded = [];
    public $primaryKey = 'id';

}
