<?php

namespace App\Domains\Auth\Models;
use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;


class CodeVerification extends Model
{
    use SqlSaveTrait;
    public $table = 'password_reset_codes';
    public $primaryKey = 'id';
    public $guarded = [];

}
