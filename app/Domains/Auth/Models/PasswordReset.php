<?php

namespace App\Domains\Auth\Models;
use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;


class PasswordReset extends Model
{
    const UPDATED_AT = null;
    use SqlSaveTrait;
    public $table = 'password_resets';
//    public $primaryKey = null;
    public $guarded = [];

}
