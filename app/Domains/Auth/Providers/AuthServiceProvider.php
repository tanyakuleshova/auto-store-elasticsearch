<?php

namespace App\Domains\Auth\Providers;

use App\Support\Domain\ServiceProvider as DomainServiceProvider;

class AuthServiceProvider extends DomainServiceProvider
{
    protected $resourceAlias = 'auth';

    protected $hasViews = true;

    protected $hasTranslations = true;

    protected $hasMigrations = true;

    protected $factories = [

    ];

    protected $subProviders = [
        RouteServiceProvider::class
    ];
}
