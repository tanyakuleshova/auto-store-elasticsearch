<?php

namespace App\Domains\Auth\Jobs;

use App\Support\Service\Mail\SendMailService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmailResetCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $passwordReset;

    public function __construct($passwordReset)
    {
        $this->passwordReset = $passwordReset;
    }

    /**
     * Execute the job.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle()
    {
        try{
            Mail::to($this->passwordReset->email)
                ->send(new SendMailService(
                        ['code'=>$this->passwordReset->code, 'subject'=>'Reset password'],
                        "auth::email_password_code")
                );

        }catch (\Exception $error){
            Log::channel('send_email')
                ->info("time: " . Carbon::now()->format('Y-m-d H:i:s')
                    . " , email: " .$this->passwordReset->email .", error: ". $error->getMessage());
        }
    }
}
