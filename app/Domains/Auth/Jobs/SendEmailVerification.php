<?php

namespace App\Domains\Auth\Jobs;

use App\Support\Service\Mail\SendMailService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmailVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $verification;

    public function __construct($verification)
    {
        $this->verification = $verification;
    }

    /**
     * Execute the job.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle()
    {
        try{
            Mail::to($this->verification->email)
                ->send(new SendMailService(
                        ['code'=>$this->verification->code, 'subject'=>'Email verification'],
                        "auth::email_verification")
                );

        }catch (\Exception $error){
            Log::channel('send_email')
                ->info("time: " . Carbon::now()->format('Y-m-d H:i:s')
                    . " , email: " .$this->verification->email .", error: ". $error->getMessage());
        }
    }
}
