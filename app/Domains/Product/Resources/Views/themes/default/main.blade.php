@extends('product::themes.default.layout')
@section('content')
    <div class="">
        @include('product::themes.default.header')

        {!! \App\Domains\Product\Services\Outputting\HtmlOutputting::main_banner() !!}
        {!! \App\Domains\Product\Services\Outputting\HtmlOutputting::main_menu() !!}

    </div>
@endsection
