<div class="flex justify-between flex-wrap max-w-screen-xl m-auto my-6">
    <?php $i=0;?>
    @foreach($menu as $category)
        @if($category->lang && $category->active)
            <a class="my-2 md:my-4 md:w-1/2 w-full "
               href="{{route('category',['id'=>$category->children->first()->id, 'link'=>\Illuminate\Support\Str::slug($category->children->first()->lang->name,'_')])}}">
                <div @if($i%2 == 0) class="md:mr-4 mx-4" @else class="md:ml-4 mx-4" @endif>
                    <div class="rounded cursor-pointer p-4 bg-gray-100">
                        <div class="flex justify-between items-center">
                            @if($category->getMedia('collection')[0] ?? '')
                                <div class="w-1/2 mr-4 md:mr-0">
                                    <img class="md:h-48" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="{{$category->getMedia('collection')[0]->getUrl()}}"
                                         alt="">
                                </div>
                            @endif
                            <div>
                                <div class="plain text-lg md:text-xl w-1/2">{{$category->id}} {{$category->lang->name}}</div>
                                <div>
                                @foreach($category->children as $children)

                                    <div href="{{route('category',['id'=>$children->id, 'link'=>\Illuminate\Support\Str::slug($children->lang->name,'_')])}}"
                                    class="plain text-lg md:text-md">{{$children->id}} {{$children->lang->name}}</div>

                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <?php $i++; ?>
        @endif

    @endforeach
</div>
