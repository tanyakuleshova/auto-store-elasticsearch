<div class="bg-gray-100 mb-8 py-8">

    <div class="md:flex items-center container mx-auto max-w-screen-xl m-auto font-bold">

        <div class="w-auto lg:w-1/3 lg:mr-8 ml-4 xl:ml-0 animate__animated animate__fadeIn animate__delay-1s plain">
            <p class="text-md xl:text-xl text-gray-600 py-8">
                {{ __("Мотоцикл дарує свободу, нові відчуття та враження.") }}
                <br><br>
                {{ __("Якщо ви так само закохані в пригоди, як і ми, то ви в правильному місці.") }}
                <br><br>
            {{ __("У нас є все, що потрібно мотоциклісту.") }}

            <p class="text-md text-gray-600 ">THE EXTREME SHOP. Be free, be yourself</p>
            </p>
        </div>
        <div class="w-auto lg:w-2/3">
            <img style="max-height: 30rem" class="animate__animated animate__fadeInLeft" src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="{{asset('images/Motorcycle-Transparent-Background.png')}}" alt="">
        </div>
    </div>

</div>
