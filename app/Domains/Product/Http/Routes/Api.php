<?php
use App\Domains\Users\Http\Controllers\UsersController;

$this->router->group(['middleware' => ['request_handler']], function ($router) {

    $this->router->group(['middleware'=> 'jwt.verify','prefix' => 'user'], function ($router) {
        $router->post('/change', [UsersController::class, 'change']); // +
        $router->patch('/change/password', [UsersController::class, 'changePassword']); // +
        $router->patch('/change/email', [UsersController::class, 'changeEmail']); //+
        $router->delete('/delete', [UsersController::class, 'delete']); // +
        $router->get('/', [UsersController::class, 'get']); // +
        $router->get('/documents', [UsersController::class, 'getDocumentsByUser']);
        $router->get('/profile', [UsersController::class, 'getProfileDataByUser']);
    });

    $this->router->get('/policy', [UsersController::class, 'getPolicy']); // +
    $this->router->get('/terms', [UsersController::class, 'getTerms']); // +

    $this->router->group(['middleware'=>'is_admin','prefix' => 'admin'], function ($router) {
        $this->router->post('/user/verify/id_card', [UsersController::class, 'verifyIDCard']); // +
        $this->router->get('/user', [UsersController::class, 'getByID']); // +
        $this->router->get('/documents', [UsersController::class, 'getDocumentsByAdmin']);
    });

});

