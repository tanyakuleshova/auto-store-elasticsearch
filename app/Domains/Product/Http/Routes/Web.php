<?php

use App\Domains\Product\Http\Controllers\CategoryController;
use App\Domains\Product\Http\Controllers\LanguageController;
use App\Domains\Product\Http\Controllers\MainController;
use App\Domains\Product\Http\Controllers\SearchController;


$this->router->get('/', [MainController::class, 'index'])->name('home');
$this->router->get('category/{id}/{link}/{filters?}', [CategoryController::class, 'index'])->name('category');
$this->router->post('category/{id}/{link}/{filters?}', [CategoryController::class, 'index']);
$this->router->get('change_lang/{lang}', [LanguageController::class, 'set_lang'])->name('lang');
$this->router->post('search', [SearchController::class, 'product_list']);
