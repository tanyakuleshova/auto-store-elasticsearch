<?php

namespace App\Domains\Product\Http\Controllers;


use App\Domains\Product\Repository\SQLRepository\CategoryRepository;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redis;

class MainController extends BaseController
{
    public function index()
    {

        $menu =  (new CategoryRepository)->menu_categories();
//        dd(App::getLocale());
        return view('product::themes/'.config('customize.theme').'/main', compact('menu'));
    }
}
