<?php

namespace App\Domains\Product\Http\Controllers;


use App\Domains\Product\Repository\SQLRepository\CategoryRepository;
use App\Domains\Product\Repository\SQLRepository\ProductRepository;

use App\Domains\Product\Services\Filtering\ElasticFiltering;
use App\Support\Http\Controller as BaseController;

class CategoryController extends BaseController
{
    public function __construct(
        public ProductRepository $productRepository,
        public CategoryRepository $categoryRepository,
        public ElasticFiltering $filterService,
    ) {}

    public function index($id, $link, $filters = false)
    {
        $category = $this->categoryRepository->getById($id);
        $products = $this->productRepository->getByParamsPaginated(['category_id'=>$id], 21);

        $all_filters = $this->filterService->getProductFilters($id);
        $applied_filters = request()->get('filters') ?? ($this->parse_url($filters) ?? []);

        if ($applied_filters || request()->get('ajax')) {

            if(!empty($applied_filters)) {
                $filtered_products_ids = $this->filterService->getFilteredProducts($applied_filters, $id);
                $products = $this->productRepository->getByIdsPaginated($filtered_products_ids, 21);
                $all_filters = $this->filterService->updateFilters($all_filters, $applied_filters, $id);
            }

            if(request()->get('ajax')){
                $product_view = view('product::themes.default.partials.products', compact('products'))->render();
                $filter_view = view('product::themes.default.partials.filters', compact('all_filters'))->render();
                return response(['success'=> true, 'view'=>$product_view, 'filters'=>$filter_view, 'data'=>$products, 'filters_data'=>$all_filters]);
            }
        }


        if (!$category || !$category->active || !$category->lang) abort(404);

        return view('product::themes/'.config('customize.theme').'/category', compact( 'category', 'products', 'all_filters'));
    }


    public function parse_url($filters): array
    {
        $applied_filters_url = [];
        if($filters){
            $filter_str = str_replace('filters-', '', $filters);
            $filters_arr = explode('-', $filter_str);

            $arr = [];
            foreach ($filters_arr as $filter){
                $ex = explode('_', $filter);
                if(count($ex) == 2){
                    $arr[$ex[0]][] = $ex[1];
                }
            }

            foreach ($arr as $k => $item){
                $b = [];
                $b['atr_id'] = $k;
                $b['value_ids'] = $item;
                $applied_filters_url[] = $b;
            }
        }
        return $applied_filters_url;
    }
}
