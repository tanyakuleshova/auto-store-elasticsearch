<?php

namespace App\Domains\Product\Http\Controllers;

use App\Support\Http\Controller as BaseController;
use Illuminate\Support\Facades\App;

class LanguageController extends BaseController
{
    public function set_lang($lang)
    {
        App::setLocale($lang);
        session()->put('locale', $lang);
        return redirect()->back();
    }
}
