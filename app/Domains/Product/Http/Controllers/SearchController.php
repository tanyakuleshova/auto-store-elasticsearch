<?php

namespace App\Domains\Product\Http\Controllers;


use App\Domains\Product\Repository\SQLRepository\ProductRepository;
use App\Domains\Product\Services\Searching\ElasticSearching;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;

class SearchController extends BaseController
{
    public function __construct(public ElasticSearching $searchService, public ProductRepository $productRepository){}

    public function product_list()
    {
        $q = \request()->get('q');
        $product_ids = $this->searchService->product_search($q);
        if(!empty($product_ids)){
            $products = $this->productRepository->getByIds($product_ids, 15);
            $view = view('themes/'.config('customize.theme').'/partials/search_results_ajax', compact( 'products'))->render();
            return response(['success'=>true, 'view'=>$view]);
        }
        return response(['success'=>false]);
    }
}
