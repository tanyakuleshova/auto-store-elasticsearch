<?php
namespace App\Domains\Product\Services\Caching;

interface CachingInterface
{
    public static function getMenu();
}
