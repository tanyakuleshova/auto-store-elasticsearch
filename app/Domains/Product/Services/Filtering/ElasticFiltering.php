<?php

namespace App\Domains\Product\Services\Filtering;

use Elastic\Elasticsearch\ClientBuilder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class ElasticFiltering
{
    protected $elasticsearch;

    public function __construct()
    {
        $this->elasticsearch = ClientBuilder::create()->setHosts([env('ELASTIC_HOST')])->build();
    }

    public function getProductFilters($category_id): array
    {
        $params = [
            'index' => 'product_updated',
            'body' => [
                'size' => 0,
                'query' => [
                    'bool' => [
                        "must" => [
                            ["match" => ["category_id" => $category_id]]
                        ]
                    ]
                ],
                'aggs'=>[
                    "attributes"=>[
                        "nested" => [
                            "path"=> "attr.ru"
                        ],
                        "aggs"=>[
                            "names"=> [
                                "terms" => [
                                    "field" => "attr.ru.attribute_name.keyword",
                                    "size" => 10000
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $attributes = $this->elasticsearch->search($params)['aggregations']['attributes']['names']['buckets'] ?? [];

        $attributes_values = [];
        $next_query_params = [];
        foreach ($attributes as $attribute){
            $tmp = [];
            $key_value = explode("*$*", $attribute['key']);
            $tmp['key'] = $key_value[1];
            $tmp['atr_id'] = $key_value[0];
            $tmp['values'] = [];
            $attributes_values[$tmp['atr_id']] = $tmp;

            $next_query_params[$tmp['atr_id']] = $this->prepareValuesParams($tmp['atr_id']);
        }


        $params = [
            'index' => 'product_updated',
            'body' => [
                'size' => 0,
                'query' => [
                    'bool' => [
                        "must" => [
                            ["match" => ["category_id" => $category_id]]
                        ]
                    ]
                ],
                "aggs" => [
                    "attributes" => [
                        "nested" => [
                            "path" => "attr.ru"
                        ],
                        "aggs" => $next_query_params
                    ]
                ]
            ]
        ];
        $values = $this->elasticsearch->search($params)['aggregations']['attributes'] ?? [];


        foreach ($values as $key => $value){
            if(isset($attributes_values[$key])){
                $buckets = $value['names']['buckets'];
                foreach ($buckets as $bucket){
                    $value_key = explode("*$*", $bucket['key']);
                    $attributes_values[$key]['values'][$value_key[0]]['name'] = $value_key[1];
                    $attributes_values[$key]['values'][$value_key[0]]['doc_count'] = $bucket['doc_count'];
                }
            }
        }

        return $attributes_values;
    }

    function prepareValuesParams($atr_id)
    {
        $param = [
            "filter" => [
                "bool" => [
                    "filter" => [
                        ["term" => ["attr.ru.id" => $atr_id]]
                    ]
                ]
            ],
            "aggs" => [
                "names" => [
                    "terms" => [
                        "field" => "attr.ru.value_name.keyword",
                        "size" => 10000
                    ]
                ]
            ]
        ];
        return $param;
    }

    function getFilteredProducts($applied_filters, $category_id): array
    {
        $query_params = $this->prepare_data($applied_filters, $category_id);

        $params = [
            'index' => 'product_updated',
            'body' => [
                'size' => 500,
                'query' => [
                    'bool' => [
                        'should' => $query_params
                    ]
                ]
            ]
        ];
        $filtered_products = $this->elasticsearch->search($params)['hits']['hits'] ?? [];
        return Arr::pluck($filtered_products, '_id');
    }

    function updateFilters($all_filters, $applied_filters, $category_id): array
    {
        $atr_values = $all_filters;
        foreach ($atr_values as $k => $atr_value){
            if(($atr_value['atr_id'] ?? '')){
                if($this->filter_exist($applied_filters, $atr_value['atr_id'])){
                    $filters = $this->remove_filter($applied_filters, $atr_value['atr_id']);
                    $query_params =  $this->prepare_data($filters, $category_id);
                }else{
                    $query_params =  $this->prepare_data($applied_filters, $category_id);
                }
//                dd($query_params);

                $params = [
                    'index' => 'product_updated',
                    'body' => [
                        'size' => 0,
                        'query' => [
                            'bool' => [
                                'should' => $query_params
                            ]
                        ],
//                        'aggs'=>[
//                            'distinctKeys'=>[
//                                'terms'=>[
//                                    'field' => "atr_".$atr_value['atr_id'].".value_id",
//                                    'size'=>500
//                                ]
//                            ]
//                        ]
                        'aggs'=>[
                            "attributes" => [
                                "nested" => [
                                    "path" => "attr.ru"
                                ],
                                "aggs" => [
                                    "values"=> [
                                        "filter" => [
                                            "bool" => [
                                                "filter" => [
                                                    ["term" => ["attr.ru.id" => $atr_value['atr_id']]]
                                                ]
                                            ]
                                        ],
                                        "aggs" => [
                                            "names" => [
                                                "terms" => [
                                                    "field" => "attr.ru.value_id",
                                                    "size" => 10000
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];
                $keys = $this->elasticsearch->search($params)['aggregations']['attributes']['values']['names']['buckets'] ?? [];

                foreach ($all_filters[$k]['values'] as $i=>$v){
                    $all_filters[$k]['values'][$i]['doc_count'] = 0;
                }
                foreach ($keys as $item){
                    $all_filters[$k]['values'][$item['key']]['doc_count'] = $item['doc_count'];
                }
            }
        }
        return $all_filters;
    }

    public function renderCombinations($applied_filters): array
    {
        $or_block = [];
        $block['must'] = [];
        $block['should'] = [];
        $or_block[] = $block;

        usort($applied_filters, function ($a, $b) {
            return (count($b['value_ids']) - count($a['value_ids']));
        });

        foreach ($applied_filters as $applied_filter) {
            $count = count($applied_filter['value_ids']);
            if ($count == 1) {
                for ($i = 0; $i < count($or_block); $i++) {
                    $or_block[$i]['must'][] = ["nested" => ["path" => "attr.ru", "query" => ["term" => ["attr.ru.atr_val" => ["value" => $applied_filter['atr_id']."*$*".$applied_filter['value_ids'][0]]]]]];
                }
            } elseif ($count > 1) {
                if (count(end($or_block)['should']) > 0) {
                    $blocks = [];
                    for ($i = 0; $i < count($or_block); $i++) {
                        foreach ($applied_filter['value_ids'] as $k => $value) {
                            $block = $or_block[$i];
                            $block['must'][] = ["nested" => ["path" => "attr.ru", "query" => ["term" => ["attr.ru.atr_val" => ["value" => $applied_filter['atr_id']."*$*".$value]]]]];
                            $blocks[] = $block;
                        }
                    }
                    $or_block = $blocks;
                } else {
                    foreach ($applied_filter['value_ids'] as $value) {
                        $or_block[0]['should'][] = ["nested" => ["path" => "attr.ru", "query" => ["term" => ["attr.ru.atr_val" => ["value" => $applied_filter['atr_id']."*$*".$value]]]]];
                    }

                }
            }
        }
        return $or_block;
    }

    public function formatDataForRequest($or_block, $id): array
    {
        $query_params = [];
        foreach ($or_block as $block) {
            $must = $block['must'];
            $must[] = ["match" => ["category_id" => $id]];
            $must[] = [
                "bool" => [
                    "should" => $block['should']
                ]
            ];
            $bool = [];
            $bool['bool']['must'] = $must;
            $query_params[] = $bool;
        }
//        dd($query_params);
        return $query_params;
    }

    public function prepare_data($applied_filters, $id): array
    {
        $combinations = $this->renderCombinations($applied_filters);
        return $this->formatDataForRequest($combinations, $id);
    }

    function filter_exist($applied_filters, $atr_id): bool
    {
        foreach ($applied_filters as $filter){
            if($filter['atr_id'] == $atr_id) return true;
        }
        return false;
    }

    function remove_filter($applied_filters, $atr_id): array
    {
        $filters = [];
        foreach ($applied_filters as $filter){
            if($filter['atr_id'] != $atr_id){
                $filters[] = $filter;
            }
        }
        return $filters;
    }
}
