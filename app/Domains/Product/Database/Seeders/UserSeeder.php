<?php

namespace App\Domains\Users\Database\Seeders;

use App\Domains\Users\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        $users = User::all();
        if(count($users) < 20){
            $this->createUsers();
        }
    }

    public function createUsers()
    {
        for($i=0; $i<10; $i++){
            $user = User::create([
                'first_name'=> fake()->firstName(),
                'last_name'=>fake()->lastName(),
                'email'=>fake()->email(),
                'password' => fake()->password(8,12),
                'role'=>User::ROLE_USER
            ]);
//            $user->assignRole('User');
        }
        for($i=0; $i<10; $i++){
            $user = User::create([
                'first_name'=> fake()->firstName(),
                'last_name'=>fake()->lastName(),
                'email'=>fake()->email(),
                'password' => fake()->password(8,12),
                'email_verified_at'=>Carbon::now(),
                'role'=>User::ROLE_ADMIN
            ]);
//            $user->assignRole('Admin');
        }
        $user = User::create([
            'first_name'=> fake()->firstName(),
            'last_name'=>fake()->lastName(),
            'email'=>'adm@admin',
            'password' => 'PassWord123',
            'role'=>User::ROLE_ADMIN
        ]);
//        $user->assignRole('Admin');
    }
}
