<?php

namespace App\Domains\Product\Repository\SQLRepository;


use App\Domains\Product\Models\Product;
use App\Domains\Product\Services\Formatting\ELProduct;
use App\Support\Repository\SQLAbstractRepository;

class ProductRepository extends SQLAbstractRepository
{
    use ELProduct;

    const MODEL = 'App\Models\Product';

    public array $relations = [
        'langs'
    ];

    public function getByParamsPaginated($params, $qty)
    {
        return Product::where($params)->paginate($qty);
    }
    public function getByIdsPaginated($ids, $qty)
    {
        return Product::whereIn('id', $ids)->paginate($qty);
    }
    public function getByIds($ids, $qty)
    {
        return Product::whereIn('id', $ids)->limit($qty)->get();
    }

}
