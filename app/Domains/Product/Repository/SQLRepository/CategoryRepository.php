<?php

namespace App\Domains\Product\Repository\SQLRepository;


use App\Domains\Product\Models\Category;
use App\Support\Repository\SQLAbstractRepository;

class CategoryRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Product\Models\Category';

    public array $relations = [
        'lang', 'products', 'children.lang', 'children.children'
    ];

    public function main()
    {
        return Category::where('id', 1)->with($this->relations)->first();
    }
    public function menu_categories()
    {
        return Category::where('parent_id', 0)->with($this->relations)->get();
    }
}
