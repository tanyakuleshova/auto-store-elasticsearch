<?php

namespace App\Domains\Product\Models;

use App\Models\Traits\LangTrait;
use App\Repository\CurrencyRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;
use Spatie\MediaLibrary\Conversions\Conversion;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ProductDesc extends Model
{
    public $table = 'product_desc';
    public $primaryKey = 'id';
    public $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
