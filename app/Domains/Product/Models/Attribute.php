<?php

namespace App\Domains\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Attribute extends Model
{
    public $table = 'attribute';
    public $primaryKey = 'id';
    public $guarded = [];

    public function lang()
    {
        return $this->hasOne(AttributeDesc::class, 'attribute_id', 'id')
            ->where('lang', App::getLocale());
    }

    public function langs()
    {
        return $this->hasMany(AttributeDesc::class, 'attribute_id', 'id');
    }

    public function options()
    {
        $options = [];
        foreach (AttributeDesc::where('lang', App::getLocale())->get() as $lang){
            $options[$lang->attribute_id] = $lang->name;
        }
        return $options;
    }

}
