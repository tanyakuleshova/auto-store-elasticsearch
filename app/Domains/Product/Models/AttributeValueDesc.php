<?php

namespace App\Domains\Product\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeValueDesc extends Model
{
    public $table = 'attribute_value_desc';
    public $primaryKey = 'id';
    public $guarded = [];

    public function attribute_value(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(AttributeValue::class);
    }
}
