<?php

namespace App\Domains\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class AttributeProduct extends Model
{
    public $table = 'attribute_product';
    public $primaryKey = 'id';
    public $guarded = [];

    public function value(){
        return $this->hasOne(AttributeValue::class, 'id', 'attribute_val_id');
    }
}
