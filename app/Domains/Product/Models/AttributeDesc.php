<?php

namespace App\Domains\Product\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeDesc extends Model
{
    public $table = 'attribute_desc';
    public $primaryKey = 'id';
    public $guarded = [];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
}
