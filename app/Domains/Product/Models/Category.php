<?php

namespace App\Domains\Product\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\Conversions\Conversion;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Category extends Model implements HasMedia
{
    use InteractsWithMedia;

    public $table = 'category';
    public $primaryKey = 'id';
    public $guarded = [];

    public function lang()
    {
        return $this->hasOne(CategoryDesc::class, 'category_id', 'id')
            ->where('category_desc.lang', App::getLocale());
    }

    public function langs()
    {
        return $this->hasMany(CategoryDesc::class, 'category_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }

    public function options()
    {
        $options = [];
        foreach (CategoryDesc::where('lang', App::getLocale())->get() as $lang){
            $options[$lang->category_id] = $lang->name;
        }
        return $options;
    }

    public function registerMediaConversions(Media $media = null):void
    {
        $this->addMediaConversion('xs')
            ->width(100)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('sm')
            ->width(200)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('md')
            ->width(600)
            ->format('webp')
            ->nonQueued();

    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('main')->singleFile();
        $this->addMediaCollection('collection');
    }
}
