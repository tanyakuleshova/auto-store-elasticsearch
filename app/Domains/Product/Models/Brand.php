<?php

namespace App\Domains\Product\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public $table = 'brand';
    public $primaryKey = 'id';
    public $guarded = [];

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}
