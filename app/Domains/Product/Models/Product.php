<?php

namespace App\Domains\Product\Models;


use App\Domains\Product\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Product extends Model implements HasMedia
{
    use InteractsWithMedia;
    use Searchable;

    public $table = 'product';
    public $primaryKey = 'id';
    public $guarded = [];

    public function attributes()
    {
        return $this->hasMany(AttributeProduct::class, 'product_id', 'id');
    }
    public function options()
    {
        $options = [];
        foreach (ProductDesc::where('lang', App::getLocale())->orderBy('id','desc')->get() as $lang){
            $options[$lang->product_id] = $lang->name . ' ('.($lang->product->reference ?? '').')';
        }
        return $options;
    }
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function option()
    {
        return $this->hasMany(OptionVal::class, 'product_id', 'id');
    }

    public function option_vals()
    {
        return $this->hasMany(OptionVal::class, 'product_id', 'id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function lang()
    {
        return $this->hasOne(ProductDesc::class, 'product_id', 'id')
            ->where('lang', App::getLocale());
    }
    public function langs()
    {
        return $this->hasMany(ProductDesc::class, 'product_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function registerMediaConversions(Media $media = null):void
    {
        $this->addMediaConversion('xs')
            ->width(100)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('sm')
            ->width(200)
            ->format('webp')
            ->nonQueued();

        $this->addMediaConversion('md')
            ->width(600)
            ->format('webp')
            ->nonQueued();

    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('main')->singleFile();
        $this->addMediaCollection('collection');
    }
}
