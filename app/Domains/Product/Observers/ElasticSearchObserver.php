<?php
namespace App\Domains\Product\Observers;

use Elastic\Elasticsearch\Client;

class ElasticSearchObserver
{
    private $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch;
    }

    public function saved($model)
    {
        $repository = "App\Repository\\" . substr(strrchr(get_class($model), "\\"), 1).'Repository';

        $this->elasticsearch->index([
//            'index' => $model->getSearchIndex(),
            'index' => 'product_updated',
            'type' => $model->getSearchType(),
            'id' => $model->getKey(),
            'body' =>  (new $repository)->instance($model->id)
        ]);
    }

    public function deleted($model)
    {
        $this->elasticsearch->index([
            'index' => $model->getSearchIndex(),
            'type' => $model->getSearchType(),
            'id' => $model->getKey(),
        ]);
    }
}
