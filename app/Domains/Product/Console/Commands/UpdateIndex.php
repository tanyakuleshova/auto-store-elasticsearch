<?php

namespace App\Domains\Product\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class UpdateIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Product update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $pr = Product::all();
        foreach ($pr as $p){
            $p->save();

        }

    }

}
