<?php

namespace App\Domains\Product\Providers;

use App\Support\Domain\ServiceProvider as DomainServiceProvider;

class ProductServiceProvider extends DomainServiceProvider
{
    protected $resourceAlias = 'product';

    protected $hasViews = true;

    protected $hasTranslations = true;

    protected $hasMigrations = true;

    protected $factories = [

    ];

    protected $subProviders = [
        RouteServiceProvider::class
    ];
}
