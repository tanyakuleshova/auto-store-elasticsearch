<?php

namespace App\Domains\Basic\Models;

use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Basic extends Model
{
    use SqlSaveTrait;
//    use Searchable;

    public $table = 'basic';
    public $primaryKey = 'id';
    public $guarded = [];

    public function toSearchableArray()
    {
        $with = [

        ];

        $this->loadMissing($with);

        return $this->toArray();
    }

}
