<?php

namespace  App\Domains\Basic\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class BasicRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Basic\Models\Basic';

    public array $relations = [

    ];
    public array $search_fields = [

    ];
}
