<?php

namespace App\Domains\Basic\Database\Seeders;

use App\Domains\Basic\Models\Basic;
use Illuminate\Database\Seeder;

class BasicSeeder extends Seeder
{
    public function run()
    {
        for($i=0; $i<=1000; $i++){
            Basic::create(['title'=> fake()->paragraph(1), 'content'=>fake()->paragraph(4)]);
        }
    }
}
