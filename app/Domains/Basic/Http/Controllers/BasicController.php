<?php

namespace App\Domains\Basic\Http\Controllers;

use App\Domains\Basic\Models\Basic;
use App\Domains\Basic\Repository\SQLRepository\BasicRepository;
use App\Domains\Basic\Services\Validation\BasicValidation;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;

class BasicController extends BaseController
{
    public function __construct(
        protected BasicRepository $repository,
        protected Basic $entity,
        protected BasicValidation $validation
    ){}

    public function index()
    {
        return response()->json(['success'=>true]);
    }
}
