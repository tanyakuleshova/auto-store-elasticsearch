<?php

use App\Domains\Basic\Http\Controllers\BasicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$this->router->middleware(['guest'])->group(function ($router) {

    $router->group(['prefix' => 'basic'], function ($router) {
        $router->get('/', [BasicController::class, 'index'])->name('basic');
    });

});
