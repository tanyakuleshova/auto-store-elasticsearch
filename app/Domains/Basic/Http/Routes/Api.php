<?php


use App\Domains\Basic\Http\Controllers\BasicController;

$this->router->group(['prefix' => 'basic'], function ($router) {
    $router->get('/get', [BasicController::class, 'index'])->middleware('jwt.verify');
});


