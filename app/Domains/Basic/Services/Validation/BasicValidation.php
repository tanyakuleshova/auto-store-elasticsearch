<?php

namespace App\Domains\Basic\Services\Validation;

use App\Support\Service\Validation\ValidationInterface;

class BasicValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
            'title'=> 'required|string|max:256'
        ];
    }
}
