<?php

namespace App\Domains\Basic\Providers;

use App\Support\Domain\ServiceProvider as DomainServiceProvider;

class BasicServiceProvider extends DomainServiceProvider
{
    protected $resourceAlias = 'basic';

    protected $hasViews = true;

    protected $hasTranslations = true;

    protected $hasMigrations = true;

    protected $factories = [

    ];

    protected $subProviders = [
        RouteServiceProvider::class
    ];
}
