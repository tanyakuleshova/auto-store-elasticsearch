<?php

namespace  App\Domains\Users\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class UserRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Users\Models\User';

    public array $relations = [
        'address', 'media', 'user_icon', 'id_card', 'leases'
    ];
    public array $search_fields = [

    ];
}
