<?php

namespace  App\Domains\Users\Repository\SQLRepository;

use App\Support\Repository\SQLAbstractRepository;

class AddressRepository extends SQLAbstractRepository
{
    const MODEL = 'App\Domains\Users\Models\Address';

    public array $relations = [

    ];
    public array $search_fields = [

    ];
}
