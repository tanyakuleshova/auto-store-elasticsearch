<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users')){
            Schema::create('users', function (Blueprint $table) {
                $table->id();
                $table->string('first_name')->nullable();
                $table->string('last_name')->nullable();
                $table->string('email')->unique();
                $table->timestamp('email_verified_at')->nullable();
                $table->string('password');
                $table->char('gender', 1)->nullable();
                $table->date('dob')->nullable();
                $table->string('phone')->nullable();
                $table->string('fax')->nullable();  // not using
                $table->timestamp('phone_verified_at')->nullable();
                $table->boolean('active')->default(0);
                $table->string('id_card_verified')->nullable();
                $table->text('twitter_id')->nullable();
                $table->text('facebook_id')->nullable();
                $table->text('fcm_token')->nullable();
                $table->string('stripe_id')->nullable();
                $table->string('role')->nullable();
                $table->index('role');
                $table->dateTime('deleted_at')->nullable();
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
    }
};
