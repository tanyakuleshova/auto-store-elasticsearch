<?php

namespace App\Domains\Users\Database\Seeders;

use App\Domains\Applications\Database\Seeders\ApplicationsSeeder;
use App\Domains\Properties\Database\Seeders\PropertySeeder;
use Elastic\Elasticsearch\ClientBuilder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $client = ClientBuilder::create()->setHosts([env('ELASTICSEARCH_HOST')])->build();
//        $params = ['index' => 'properties'];
//        $client->indices()->delete($params);

//        Schema::drop('media');
//        Artisan::call('migrate:refresh');
        $this->call(PermissionsSeeder::class);
//        $this->call(UserSeeder::class);
        $this->call(PropertySeeder::class);
//        $this->call(ApplicationsSeeder::class);
    }
}
