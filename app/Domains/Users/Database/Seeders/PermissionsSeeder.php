<?php

namespace App\Domains\Users\Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Permission::exists()){
            try{
                Permission::create(['name' => 'super']);
                Permission::create(['name' => 'admin']);
                Permission::create(['name' => 'user']);
            }catch(\Exception $e){}



            $superAdminRole = Role::create(['name' => 'Super Admin']);
            $adminRole = Role::create(['name' => 'Admin']);
            $userRole = Role::create(['name' => 'User']);

            try{
                $superAdminRole->givePermissionTo([
                    'super'
                ]);

                $adminRole->givePermissionTo([
                    'admin'
                ]);

                $userRole->givePermissionTo([
                    'user'
                ]);
            }catch(\Exception $e){}
        }
    }
}
