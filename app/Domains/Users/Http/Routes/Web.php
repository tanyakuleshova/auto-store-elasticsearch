<?php


use App\Domains\Users\Http\Controllers\UsersController;

$this->router->get('/user/index', [UsersController::class, 'index']);
