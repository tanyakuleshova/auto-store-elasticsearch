<?php

namespace App\Domains\Users\Http\Controllers;

use App\Domains\Users\Services\ProfileService;
use App\Domains\Users\Services\UserService;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;

/**
 * @OA\PathItem(
 *      path="/api/user/change"
 *  ),
 * @OA\POST(path="/api/user/change",
 *     security={{ "apiAuth": {} }},
 *     tags={"user"},
 *     summary="Route for changing general user information",
 *     description="
 *     This endpoint is to modify user data.",
 *     @OA\RequestBody(
 *      required=true,
 *     @OA\MediaType(
 *       mediaType="multipart/form-data",
 *       @OA\Schema(
 *        @OA\Property(property="first_name", type="string", description="Allowed characters are: letters and dashes.", example="Alex", maxLength=255),
 *        @OA\Property(property="last_name", type="string", description="Allowed characters are: letters.", example="Brave", maxLength=255),
 *        @OA\Property(property="dob", type="string", description="Format: Y-m-d.", example="1993-08-01"),
 *        @OA\Property(property="gender", type="string", description="Possible values are: m, f. Where m - male, f - female.", example="m", maxLength=1),
 *        @OA\Property(property="phone", type="string", example="971527630345", description="Allowed characters are: dashes, +, numbers.", minLength=10, maxLength=18),
 *        @OA\Property(property="fax", type="string", description="Fax", example="971527630345"),
 *
 *        @OA\Property(property="user_icon", description="Main user photo. Only one file is allowed. Possible formats are: png, jpeg, jpg, gif, svg, webp, heic.", type="file", @OA\Schema(type="file")),
 *        @OA\Property(property="id_card[]", description="ID card files.", type="array", @OA\Items(type="file")),
 *         @OA\Property(property="address", type="object",
 *                      @OA\Property(property="country", type="string", example="UAE"),
 *                      @OA\Property(property="city", type="string", example="Dubai"),
 *                      @OA\Property(property="street", type="string", example="1st Highway, 23"),
 *                      @OA\Property(property="zip_code", type="string", example="67-65F"),
 *              ),

 *    )
 * )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="obj", example={ "id": 395, "first_name": "Alex", "last_name": "Brave", "email": "t@test", "email_verified_at": "2022-09-05T10:26:18.000000Z", "gender": "m", "dob": "1993-08-01", "phone": null, "phone_verified_at": null, "active": 1,"facebook_id": null, "twitter_id": null, "deleted_at": null, "address": { "id": 7, "user_id": 395, "country": "UAE", "city": "Dubai", "street": "1st Highway, 23", "zip_code": "67-65F"},
"media": {{ "id": 85, "collection_name": "id_card", "file_name": "file.pdf", "generated_conversions": {}, "order_column": 1, "original_url": "http://127.0.0.1:8000/media/85/85/file.pdf" },
{"id": 99,"collection_name": "id_card","file_name": "Flowchart.jpg","generated_conversions": {"md": true,"sm": true,"xs": true},"order_column": 2,"original_url": "http://127.0.0.1:8000/media/99/99/doc.jpg"},
 {"id": 98, "collection_name": "user_icon", "file_name": "Flowchart.jpg", "generated_conversions": { "md": true, "sm": true, "xs": true}, "order_column": 1, "original_url": "http://127.0.0.1:8000/media/98/98/Flowchart.jpg"}}}),
 *    )
 * ),
 *     @OA\Response(response="422", description="validation error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example={ "last_name": {"The last name must only contain letters."},"dob": {"The dob is not a valid date.","The dob does not match the format Y-m-d."}}))
 *     )
 * ),
 *  * @OA\PathItem(
 *      path="/api/user"
 *  ),
 * @OA\Get (path="/api/user",
 *     tags={"user"},
 *     summary="Route for getting user object.",
 *     security={{ "apiAuth": {} }},
 *     description="
 *     This route returns a user object with related objects like address, user_icon, id_card.",
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *      *        @OA\Property(property="user", type="obj", example={ "id": 395, "first_name": "Alex", "last_name": "Brave", "email": "t@test", "email_verified_at": "2022-09-05T10:26:18.000000Z", "gender": "m", "dob": "1993-08-01", "phone": null, "phone_verified_at": null, "active": 1,"facebook_id": null, "twitter_id": null, "deleted_at": null, "address": { "id": 7, "user_id": 395, "country": "UAE", "city": "Dubai", "street": "1st Highway, 23", "zip_code": "67-65F"},
"user_icon": {{"id": 45,"collection_name": "user_icon","file_name": "s-l400.png","generated_conversions": {},"order_column": 1,"original_url": "http://seer_be.local/media/45/45/s-l400.png"}},
"id_card": {{"id": 2,"collection_name": "id_card","file_name": "korea_624.jpg","generated_conversions": {"md": true,"sm": true,"xs": true},"order_column": 2,"original_url": "https://apidevseer.eivolo.com/media/02/2/korea_624.jpg"}}
 *     }),
 *    )
 * ),
 *     @OA\Response(response="422", description="User not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="user_not_found")
 *     )
 * ),
 * )
 *  @OA\PathItem(
 *      path="/api/user/delete"
 *  ),
 * @OA\Delete (path="/api/user/delete",
 *     tags={"user"},
 *     summary="Route for deleting user",
 *     description="
 *     This endpoint is for deleting a user.
 *     User will be soft deleted and logged out.
 *     After that, the user will not be able to login.
 *     If the user registers with the same email, the account will be activated again.
 *     Email should be verified again.",
 *      security={{ "apiAuth": {} }},
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true)
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * )
 * )
 *  @OA\PathItem(
 *      path="/api/user/change/password"
 *  ),
 * @OA\Patch (path="/api/user/change/password",
 *     tags={"user"},
 *     summary="Route for changing password",
 *     description="
 *     Route for changing password. Pass password (old one) and new_password.",
 *      security={{ "apiAuth": {} }},
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"password", "new_password"},
 *        @OA\Property(property="password", type="string", example="Test1password"),
 *        @OA\Property(property="new_password", type="string", description="Allowed format: min 8 character, min 1 upper letter, min 1 lower letter, min 1 number", example="TestTest1",  minLength=8),
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true)
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * ),
 *     @OA\Response(response="422", description="validation_error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example={"new_password": { "The new password format is invalid.", "The new password must be at least 8 characters."}})
 *     )
 * )
 * )
 *  @OA\PathItem(
 *      path="/api/user/change/email"
 *  ),
 * @OA\Patch (path="/api/user/change/email",
 *     tags={"user"},
 *     summary="Route for changing email",
 *     description="
 *
 *     This endpoint sends a code to the user.
 *     Pass new email as parameter.
 *     Old email will not be changed until the user confirms the new email.
 *     To confirm email use /api/auth/verification/email/verify",
 *      security={{ "apiAuth": {} }},
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"email"},
 *        @OA\Property(property="email", type="string", example="ttt@rrr.mail")
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="null", example="null")
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * ),
 *     @OA\Response(response="422", description="validation_error",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="validation_error"),
 *        @OA\Property(property="errors", type="obj", example={"email": { "The email field is required." }})
 *     )
 * )
 * ),
 *  @OA\PathItem(
 *      path="/api/admin/user/verify/id_card"
 *  ),
 * @OA\Post(path="/api/admin/user/verify/id_card",
 *     tags={"admin user"},
 *     summary="Route for verifing ID card.",
 *     description="
 *     After the user upload ID card media files, these files should be verified by administrator.
 *     Default value of id_card_verified is null. After files were uploading id_card_verified = 'Verifying', it means admin needs to verify it.
 *     Admin can change id_card_verified value to 'Approved' or 'Denied'.
 *     You need to pass user ID as id parameter and the status of id_card ('Approved' or 'Denied').",
 *      security={{ "apiAuth": {} }},
 *     @OA\RequestBody(
 *         required=true,
 *      @OA\JsonContent(
 *        type="object",
 *        required={"id", "id_card_verified"},
 *        @OA\Property(property="id", type="int", description="ID of user", example="1"),
 *        @OA\Property(property="id_card_verified", type="string", example="Approved")
 *    )
 *   ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="obj", example={"id": 1, "first_name": "Julianne", "last_name": "Jones", "email": "vhettinger@simonis.com", "email_verified_at": "2022-09-10", "gender": null, "dob": null, "phone": null, "phone_verified_at": null, "active": 1, "id_card_verified": "Approved","facebook_id": null,"twitter_id": null,"fcm_token": null,"deleted_at": null,"address": { "id": 3, "user_id": 1, "country": "UAE", "city": "Dubai", "street": "1st Highway, 23", "zip_code": "67-65F"},"media": {{"id": 164,"collection_name": "id_card","file_name": "download.jpeg", "original_url": "http://seer_be.local/media/64/164/download.jpeg"}}}),
 *        @OA\Property(property="notification_error", type="string", example="fcm_token_not_found"),
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * ),
 *     @OA\Response(response="422", description="user_not_found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="user_not_found"),
 *        @OA\Property(property="message", type="string", example="The user was not found for the specified ID.")
 *     )
 * )
 * ),
 *  @OA\PathItem(
 *      path="/api/admin/user"
 *  ),
 * @OA\GET(path="/api/admin/user",
 *     tags={"admin user"},
 *     summary="Route for fetching user by ID.",
 *     description="
 *    Route for fetching user by ID.",
 *      security={{ "apiAuth": {} }},
 *     @OA\Parameter(name="id", description="User ID. Integer type. Required", in="query"),
 *     @OA\Parameter(name="with", description="Specified relationships (multiple comma-separated values allowed). Possible values are: 'leases.property', 'user_icon', 'lease' Default is null.", in="query"),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="user", type="obj", example={}),
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * ),
 *     @OA\Response(response="422", description="user_not_found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="user_not_found"),
 *        @OA\Property(property="message", type="string", example="The user was not found for the specified ID.")
 *     )
 * )
 * ),
 *  @OA\PathItem(
 *      path="/api/user/documents"
 *  ),
 * @OA\GET(path="/api/user/documents",
 *     tags={"user"},
 *     summary="Route for fetching all user documents.",
 *     description="
 *     This endpoint returns documents object that consists of: user_icon, id_card, comments_media collections.
 *     To obtain a non-empty collection of documents, you must first download these documents.
 *     To upload media use /api/media/user/upload endpoint.",
 *      security={{ "apiAuth": {} }},
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="documents", type="obj", example={
 *     "user_icon": {{"id": 45,"collection_name": "user_icon","file_name": "s-l400.png","generated_conversions": {},"order_column": 1,"original_url": "http://seer_be.local/media/45/45/s-l400.png"}},
 *     "id_card": {{"id": 2,"collection_name": "id_card","file_name": "korea_624.jpg","generated_conversions": {"md": true,"sm": true,"xs": true},"order_column": 2,"original_url": "https://apidevseer.eivolo.com/media/02/2/korea_624.jpg"}},
 *     "comments_media": {{"id": 51,"collection_name": "images","file_name": "images.jpeg","generated_conversions": {"md": true,"sm": true,"xs": true},"order_column": 1,"original_url": "http://seer_be.local/media/51/51/images.jpeg"},{"id": 55,"collection_name": "images","file_name": "images.jpeg","generated_conversions": {},"order_column": 1,"original_url": "http://seer_be.local/media/55/55/images.jpeg"}},
 *     }),
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * ),
 *     @OA\Response(response="422", description="user_not_found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="user_not_found"),
 *        @OA\Property(property="message", type="string", example="The user was not found for the specified ID.")
 *     )
 * )
 * ),
 *  @OA\PathItem(
 *      path="/api/admin/documents"
 *  ),
 * @OA\GET(path="/api/admin/documents",
 *     tags={"admin user"},
 *     summary="Route for fetching all admin documents.",
 *     description="
 *     This endpoint returns documents object that consists of: admin_icon, admin_id_card, leases_media, comments_media, tenants_media collections.
 *     This endpoint only returns the collections specified in the 'with' parameter.
 *     To filter documents by tenant specify tenant ID.
 *     To fetch all possible media, request will be: api/admin/documents?with=leases_media,report_media,comments_media,admin_icon,admin_id_card,tenants_maintenance,tenants_user_icons,tenants_id_cards,tenants_comments_media",
 *      security={{ "apiAuth": {} }},
 *     @OA\Parameter(name="with", description="Possible values are leases_media, comments_media, report_media, admin_icon, admin_id_card, tenants_maintenance, tenants_user_icons, tenants_id_cards, tenants_comments_media. Required.", in="query"),
 *     @OA\Parameter(name="filter_by_tenant_id", description="Specify tenant ID to filer documents by tenant. Nullable.", in="query"),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),
 *        @OA\Property(property="documents", type="obj", example={
 *     "admin_icon": {{"id": 45,"collection_name": "user_icon","file_name": "s-l400.png","generated_conversions": {},"order_column": 1,"original_url": "http://seer_be.local/media/45/45/s-l400.png"}},
 *     "admin_id_card": {{"id": 2,"collection_name": "id_card","file_name": "korea_624.jpg","generated_conversions": {"md": true,"sm": true,"xs": true},"order_column": 2,"original_url": "https://apidevseer.eivolo.com/media/02/2/korea_624.jpg"}},
 *     "comments_media": {{"id": 51,"collection_name": "images","file_name": "images.jpeg","generated_conversions": {"md": true,"sm": true,"xs": true},"order_column": 1,"original_url": "http://seer_be.local/media/51/51/images.jpeg"},{"id": 55,"collection_name": "images","file_name": "images.jpeg","generated_conversions": {},"order_column": 1,"original_url": "http://seer_be.local/media/55/55/images.jpeg"}},
 *     "leases_media": {{"id": 256,"collection_name": "docs","file_name": "Screenshot-from-2022-10-30-10-15-37.png","order_column": 1,"original_url": "http://seer_be.local/media/56/256/Screenshot-from-2022-10-30-10-15-37.png","xs_url": "http://seer_be.local/media/56/256/Screenshot-from-2022-10-30-10-15-37.png?w=300","md_url": "http://seer_be.local/media/56/256/Screenshot-from-2022-10-30-10-15-37.png?w=600","lg_url": "http://seer_be.local/media/56/256/Screenshot-from-2022-10-30-10-15-37.png?w=1200"},
 *     { "id": 257,"collection_name": "docs","file_name": "Screenshot-from-2022-10-30-10-15-37.png","order_column": 1,"original_url": "http://seer_be.local/media/57/257/Screenshot-from-2022-10-30-10-15-37.png","xs_url": "http://seer_be.local/media/57/257/Screenshot-from-2022-10-30-10-15-37.png?w=300","md_url": "http://seer_be.local/media/57/257/Screenshot-from-2022-10-30-10-15-37.png?w=600","lg_url": "http://seer_be.local/media/57/257/Screenshot-from-2022-10-30-10-15-37.png?w=1200"}},
 *     "tenants_media": {
 *     "maintenance_media": {"id": 19,"collection_name": "attachments","file_name": "181015194322-03-mandi-jackson-hurricane-damage-exlarge-169.jpg","order_column": 1,"original_url": "http://seer_be.local/media/19/19/181015194322-03-mandi-jackson-hurricane-damage-exlarge-169.jpg","xs_url": "http://seer_be.local/media/19/19/181015194322-03-mandi-jackson-hurricane-damage-exlarge-169.jpg?w=300","md_url": "http://seer_be.local/media/19/19/181015194322-03-mandi-jackson-hurricane-damage-exlarge-169.jpg?w=600","lg_url": "http://seer_be.local/media/19/19/181015194322-03-mandi-jackson-hurricane-damage-exlarge-169.jpg?w=1200"}
 *     }
 *     }),
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * ),
 *     @OA\Response(response="422", description="user_not_found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="user_not_found"),
 *        @OA\Property(property="message", type="string", example="The user was not found for the specified ID.")
 *     )
 * )
 * ),
 *  @OA\PathItem(
 *      path="/api/user/profile"
 *  ),
 * @OA\GET(path="/api/user/profile",
 *     tags={"user"},
 *     summary="Route for fetching all user data for profile.",
 *     description="
 *     This endpoint returns the last two maintenance requests, the last two applications, the last five leases and reports.",
 *      security={{ "apiAuth": {} }},
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),

 *     @OA\Property(property="applications", type="obj", example={
 {"id": 1,"user_id": 2,"property_id": 1,"status": "Approved","annual_income": 2861265,"duration": 1,"start_date": "2020-07-05","end_date": "2021-07-05","created_at": "2022-10-05T15:44:11.000000Z",
 * "property": {"id": 74,"admin_id": 2,"name": "House Wilmer Rest","description": "Quia non expedita nesciunt non et placeat. Dicta suscipit iure adipisci ab magni non. Sequi eum vel itaque sint vitae aut minus.","type_id": 10,"address": "847 Terry Points","city": "Dubai","status": "Active","price_per_yr": 2958322,"bedroom_qty": 4,"bathroom_qty": 7,"room_qty": 9,"area_sq": 423,"deleted_at": null}}
 *     }),
 *     @OA\Property(property="leases", type="obj", example={{"id": 52,"admin_id": 2,"application_id": 52,"property_id": 52,"status": "Active","start_date": "2022-09-24","end_date": "2023-09-24","annual_income": 2618021,"deleted_at": null, "property": {"id": 52,"admin_id": 2,"name": "House Ortiz Falls","description": "Sint facere ut magnam rerum. Iste optio laudantium enim est ut. Quo temporibus quia atque consectetur ad facilis earum. Sit et odit omnis qui non qui sint.","type_id": 8,"address": "43894 Karina Track Suite 693","city": "Dubai","status": "Active","price_per_yr": 1779866,"bedroom_qty": 10,"bathroom_qty": 3,"room_qty": 6,"area_sq": 232,"deleted_at": null,
 *     "main_image": {{"id": 259,"collection_name": "main","file_name": "pexels-photo-3221215.jpeg","generated_conversions": {},"order_column": 1,"original_url": "http://seer_be.local/media/59/259/pexels-photo-3221215.jpeg"}},
"rooms": {{"id": 52,"property_id": 52,"name": "Kids Bedroom","media": {{"id": 260,"collection_name": "images","file_name": "flying-pendant-lights-colourful-stools-kitsch-living-room.jpg","generated_conversions": {"md": true,"sm": true,"xs": true},"order_column": 1,"original_url": "http://seer_be.local/media/60/260/flying-pendant-lights-colourful-stools-kitsch-living-room.jpg"}}}},
"amenities": {{"id": 2,"name": "Swimming Pool","sort": 2,"active": 1,"deleted_at": null,"laravel_through_key": 52}},
"facilities": {{"id": 4,"name": "Washing machine","sort": 4,"active": 1,"deleted_at": null,"created_at": "2022-10-24T07:57:15.000000Z","updated_at": "2022-10-24T07:57:15.000000Z","laravel_through_key": 52}},
"services": {{"id": 2,"name": "Gas","sort": 2,"active": 1,"deleted_at": null,"created_at": "2022-10-24T07:57:15.000000Z","updated_at": "2022-10-24T07:57:15.000000Z","laravel_through_key": 52}}
 *     }}}),
 *     @OA\Property(property="maintenances", type="obj", example={
{"id": 22,"description": "Test","property_id": 52,"user_id": 3,"type_id": 5,"status": "New","created_at": "2022-10-24T08:01:51.000000Z","type": {"id": 5,"name": "Wall Paint","sort": "5",
"media": {{"id": 5,"collection_name": "icon","file_name": "wall_paint.svg","generated_conversions": {},"order_column": 1,"original_url": "http://seer_be.local/media/05/5/wall_paint.svg"}}}}
 *     })
 *    )
 * ),
 *     @OA\Response(response="401", description="invalid token, token not found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="token_not_found")
 *     )
 * ),
 *     @OA\Response(response="422", description="user_not_found",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=false),
 *        @OA\Property(property="error", type="string", example="user_not_found"),
 *        @OA\Property(property="message", type="string", example="The user was not found for the specified ID.")
 *     )
 * )
 * ),
 *  @OA\PathItem(
 *      path="/api/policy"
 *  ),
 * @OA\GET(path="/api/policy",
 *     tags={"public"},
 *     summary="Route for fetching privacy policy text.",
 *     description="
 *     Route for fetching privacy policy text.",
 *
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),

 *     @OA\Property(property="body", type="obj", example="Lorem ipsum...")
 *    )
 * )
 * ),
 * *  @OA\PathItem(
 *      path="/api/terms"
 *  ),
 * @OA\GET(path="/api/terms",
 *     tags={"public"},
 *     summary="Route for fetching terms of service text.",
 *     description="
 *     Route for fetching terms of service text.",
 *
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *     @OA\JsonContent(
 *        type="object",
 *        @OA\Property(property="success", type="bool", example=true),

 *     @OA\Property(property="body", type="obj", example="Lorem ipsum...")
 *    )
 * )
 * ),
 */
class UsersController extends BaseController
{
    public function change(Request $request, UserService $userService): \Illuminate\Http\JsonResponse
    {
        return $userService->changeUserData($request);
    }

    public function changePassword(Request $request, UserService $userService): \Illuminate\Http\JsonResponse
    {
        return $userService->updatePassword($request);
    }

    public function changeEmail(Request $request, UserService $userService): \Illuminate\Http\JsonResponse
    {
        return $userService->changeEmail($request);
    }

    public function delete(UserService $userService): \Illuminate\Http\JsonResponse
    {
        return $userService->deleteUser();
    }

    public function get(UserService $userService): \Illuminate\Http\JsonResponse
    {
        return $userService->getUser();
    }

    public function getByID(Request $request, UserService $userService): \Illuminate\Http\JsonResponse
    {
        return $userService->getByID($request);
    }

    public function verifyIDCard(Request $request, UserService $userService): \Illuminate\Http\JsonResponse
    {
        return $userService->verifyIDCardByAdmin($request);
    }

    public function getDocumentsByUser(ProfileService $profileService): \Illuminate\Http\JsonResponse
    {
        return $profileService->getUserDocuments();
    }
    public function getDocumentsByAdmin(ProfileService $profileService, Request $request): \Illuminate\Http\JsonResponse
    {
        return $profileService->getAdminDocuments($request);
    }

    public function getProfileDataByUser(ProfileService $profileService, Request $request): \Illuminate\Http\JsonResponse
    {
        return $profileService->getUserProfileData($request);
    }

    public function getPolicy(): \Illuminate\Http\JsonResponse
    {
        return response()->json(['success'=>true, 'body'=>'Lorem Ipsum']);
    }
    public function getTerms(): \Illuminate\Http\JsonResponse
    {
        return response()->json(['success'=>true, 'body'=>'Lorem Ipsum']);
    }
}

