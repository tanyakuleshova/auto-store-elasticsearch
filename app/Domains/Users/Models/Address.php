<?php

namespace App\Domains\Users\Models;
use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * @OA\Property(property="user_id", type="int", description="User ID", readOnly="false"),
 * @OA\Property(property="country", type="string", description="Country name", readOnly="false"),
 * @OA\Property(property="city", type="string", description="City name", readOnly="false"),
 * @OA\Property(property="street", type="string", description="Address information (street, house or flat)", readOnly="false"),
 * @OA\Property(property="zip_code", type="string", description="Zip code", readOnly="false"),
 * @OA\Property(property="created_at", type="string",  description="Format: Y-m-d, UTC. Example of format is: 2022-08-30T13:16:52.000000Z", readOnly="true"),
 * @OA\Property(property="updated_at", type="string",  description="Format: Y-m-d, UTC. Example of format is: 2022-08-30T13:16:52.000000Z", readOnly="true"),
 * )
 *
 * @package App\Models
 */
class Address extends Model
{
    use SqlSaveTrait;
//    use Searchable;

    public $table = 'address';
    public $primaryKey = 'id';
    public $guarded = [];
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
