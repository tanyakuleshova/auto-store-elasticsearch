<?php

namespace App\Domains\Users\Models;
use App\Domains\Chat\Models\PropertyChat;
use App\Domains\Comments\Models\Comment;
use App\Domains\Leases\Models\Lease;
use App\Domains\Leases\Models\LeaseTenant;
use App\Domains\Maintenance\Models\Maintenance;
use App\Support\Service\Traits\SqlSaveTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Scout\Searchable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @OA\Schema(
 * @OA\Property(property="first_name", type="string",  description="Allowed letters and dashes.", readOnly="false"),
 * @OA\Property(property="last_name", type="string", description="String. Max size 255.", readOnly="false"),
 * @OA\Property(property="email", type="string", description="User email", readOnly="false"),
 * @OA\Property(property="email_verified_at", type="string", description="Format: Y-m-d, UTC. Example of format is: 2022-08-30T13:16:52.000000Z.", readOnly="true"),
 * @OA\Property(property="password", type="string",  description="Min one capital letter, one lowercase letter, one number. Size 8-255 symbols", readOnly="false"),
 * @OA\Property(property="gender", type="string", description="Allowed values: m, f. Where m - male, f - femail", readOnly="false"),
 * @OA\Property(property="dob", type="string", description="Date of birth. Format: Y-m-d. Example: 1993-01-08", readOnly="false"),
 * @OA\Property(property="code", type="string",  description="Allowed only numbers and +.", readOnly="false"),
 * @OA\Property(property="phone", type="string",  description="Allowed only numbers and -. Min size 7, max size 18 numbers. Phone will be sanitized and saved in the format: 337373733", readOnly="false"),
 * @OA\Property(property="phone_verified_at", type="string", description="Format: Y-m-d, UTC. Example of format is: 1993-08-30T13:16:52.000000Z", readOnly="true"),
 * @OA\Property(property="active", type="bool",  description="Show if user account is active", readOnly="true"),
 * @OA\Property(property="twitter_id", type="string",  description="User facebook token", readOnly="false"),
 * @OA\Property(property="facebook_id", type="string",  description="User twitter token", readOnly="false"),
 * @OA\Property(property="fcm_token", type="string",  description="User fcm token (firebase)", readOnly="false"),
 * @OA\Property(property="role", type="string",  description="User role. Possible values are: Admin, User, Super.", readOnly="false"),
 * @OA\Property(property="created_at", type="string", description="Format: Y-m-d, UTC. Example of format is: 2022-08-30T13:16:52.000000Z", readOnly="true"),
 * @OA\Property(property="updated_at", type="string", description="Format: Y-m-d, UTC. Example of format is: 2022-08-30T13:16:52.000000Z", readOnly="true"),
 * @OA\Property(property="deleted_at", type="string", description="Format: Y-m-d, UTC. Example of format is: 2022-08-30T13:16:52.000000Z", readOnly="true")
 * )
 *
 * @package App\Models
 */
class User extends Authenticatable implements JWTSubject, HasMedia
{

    use SqlSaveTrait;
    use InteractsWithMedia;
    use HasRoles;
    protected string $guard_name = 'web';


    public static function PHONE_VALIDATION($isRequired=true): array
    {
        $required = $isRequired ? 'required': 'nullable';
        return [$required, 'regex:' . User::PHONE_REGEX, 'min:7', 'max:18'];
    }

    public static function COUNTRY_CODE_VALIDATION($isRequired=true): array
    {
        $required = $isRequired ? 'required': 'nullable';
        return [$required, 'regex:' . User::PHONE_REGEX, 'min:2', 'max:4'];
    }
    public const PASSWORD_REGEX = '/^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*)$/';
    public const PHONE_REGEX = "/^[\\d\-+]+$/"; //numeric, +, -
    public const NAME_REGEX = "/^[A-Za-z-]*$/"; //alpha with dash
//    public const USER_PHONE = ['nullable', 'regex:' . self::PHONE_REGEX, 'min:9', 'max:18'];

    public const ID_CARD_VERIFIED = 'Approved';
    public const ID_CARD_VERIFICATION_FAILED = 'Denied';
    public const ID_CARD_WAITING_FOR_VERIFICATION = 'Verifying';


    public const ROLE_USER = 'User';
    public const ROLE_ADMIN = 'Admin';
    public const ROLE_SUPER = 'Super';

    public $table = 'users';
    public $primaryKey = 'id';
    public $guarded = [];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
    ];

    public static function sanitizePhoneNumber($number)
    {
        if(strlen($number) > 1){
            $number = str_replace(['-', ' ', '+'],'', $number);
//
//            $first_one = substr($number,0,1);
//            $first_two = substr($number,0,2);
//            if($first_two == '00'){
//                $number = substr($number, 2);
//            }elseif($first_one == '0'){
//                $number = substr($number, 1);
//            }
//            return '+'.$number;
            return $number;
        }
    }

    public static function sanitizePhoneCountryCode($number)
    {
        if(strlen($number) > 1){
            $number = str_replace(['-', ' ', '+'],'', $number);
//
//            $first_one = substr($number,0,1);
//            $first_two = substr($number,0,2);
//            if($first_two == '00'){
//                $number = substr($number, 2);
//            }elseif($first_one == '0'){
//                $number = substr($number, 1);
//            }
            return '+'.$number;
        }
    }

    public function isUser(): bool
    {
        return ($this->role == self::ROLE_USER);
    }

    public function isAdmin(): bool
    {
        return ($this->role == self::ROLE_ADMIN);
    }

    public function isSuper(): bool
    {
        return ($this->role == self::ROLE_SUPER);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
//    public function setPhoneAttribute($value)
//    {
//        $this->attributes['phone'] = $this::sanitizePhoneNumber($value);
//    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public function address(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Address::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('user_icon')->singleFile();
        $this->addMediaCollection('id_card');
        $this->addMediaCollection('images');
    }

    public static function mediaCollections(): array
    {
        return ['user_icon', 'id_card', 'images'];
    }

    public function user_icon()
    {
        return $this->media()->where('collection_name', 'user_icon');
    }

    public function id_card()
    {
        return $this->media()->where('collection_name', 'id_card');
    }

    public function maintenance()
    {
        return $this->hasMany(Maintenance::class, 'user_id', 'id');
    }


    public function comments()
    {
        return $this->hasMany(Comment::class, 'author_id', 'id');
    }

    public function leases()
    {
        return $this->hasManyThrough(Lease::class,LeaseTenant::class,
            'user_id', 'id', 'id', 'lease_id')
            ->where('leases.status', 'Active');
    }

    public function lease()
    {
        return $this->hasOneThrough(Lease::class,LeaseTenant::class,
            'user_id', 'id', 'id', 'lease_id')
            ->where('leases.status', 'Active')->orderBy('start_date');
    }

    public function admin_leases(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Lease::class, 'admin_id', 'id');
    }

    public function property_chats(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PropertyChat::class, 'user_id', 'id')
            ->orderBy('property_chats.created_at');
    }
}
