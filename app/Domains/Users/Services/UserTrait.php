<?php

namespace App\Domains\Users\Services;

use App\Domains\Media\Services\UploaderService;
use App\Domains\Users\Models\Address;
use App\Domains\Users\Models\User;

trait UserTrait
{
    public function updateAddress($request, $user): void
    {
        if($user){
            $address = $request->address ?? '';

            if($address && is_string($address)){
                $address = json_decode($address, true);
            }

            if(!empty($address)){
                $address['user_id'] = $user->id;

                if($user->address){
                    $address['id'] = $user->address->id;
                }

                Address::store($address);
            }
        }
    }

    public function saveFiles($request, $user): array
    {
        $errors = [];

        try {
            if ($request->hasFile('id_card')) {
                $user->id_card_verified = $user->role === User::ROLE_ADMIN ? User::ID_CARD_VERIFIED : User::ID_CARD_WAITING_FOR_VERIFICATION;
                $user->save();
                (new UploaderService)->uploadFilesFromRequest($user, "id_card", 'id_card');
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }

        try {
            if ($request->hasFile('user_icon') && $request->file('user_icon')->isValid()) {
                (new UploaderService)->uploadFilesFromRequest($user, "user_icon", 'user_icon');
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }

        return $errors;
    }
}
