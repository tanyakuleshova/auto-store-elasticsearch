<?php

namespace App\Domains\Users\Services;

use App\Domains\Applications\Repository\SQLRepository\ApplicationRepository;
use App\Domains\Applications\Services\ApplicationSearchService;
use App\Domains\Leases\Repository\SQLRepository\LeaseRepository;
use App\Domains\Leases\Services\LeaseSearchParams;
use App\Domains\Leases\Services\LeaseSearchService;
use App\Domains\Maintenance\Repository\SQLRepository\MaintenanceRepository;
use App\Domains\Reports\Models\Report;
use App\Domains\Reports\Repository\SQLRepository\ReportRepository;
use App\Domains\Reports\Services\ReportSearchService;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use Illuminate\Http\Request;

class ProfileService
{
    public $user;
    public $userRepository;
    public $applicationService;
    public $leaseService;

    public function __construct(
    ){
        $this->user = request()->userObj;
        if(!$this->user){
            return response()->json(['success'=>false, 'error'=>'Unauthorized'], 422);
        }
        $this->userRepository = new ProductRepository();
        $this->applicationService =  new ApplicationSearchService();
        $this->leaseService =  new LeaseSearchService();
    }
    use UserTrait;

    /* controller's methods */
    public function getUserProfileData($request)
    {
        if($this->user){
            $user = $this->userRepository->firstByParams(['id'=>$this->user->id]);
            if($user){

                $errors = [];

                $maintenances = (new MaintenanceRepository)->limitByParams(['user_id'=> $user->id], 2, ['type.media']);

                $applications = (new ApplicationRepository)->limitByParams(['user_id'=> $user->id], 2, ['property']);

                $leases = $this->leaseService->getByUser(new Request(
                    ['with' => 'reports, property.main_image, property.amenities, property.facilities, property.services',
                        'sort' => 'newest', 'max_qty'=>5, 'not_paginate'=>true, 'userObj'=>$this->user]));
                $leases = json_decode($leases->getContent());
                if(!$leases->success){$errors[] = $leases->error;}
                $leases = $leases->success ? $leases->leases : [];

                $request->merge(['limit'=>5, 'sort'=>'newest', 'with'=>'notifications']);
                $reportsResponse = (new ReportSearchService)->getReportsByUser($request);
                $res = json_decode($reportsResponse->getContent());
                $reports = $res->success ? $res->reports->data : [];

                if(!empty($errors)){
                    return response()->json(['success' => false, 'errors'=>$errors, 'applications'=>$applications, 'leases'=>$leases, 'maintenances'=>$maintenances, 'reports'=>$reports]);
                }
                return response()->json(['success' => true,  'reports'=>$reports, 'applications'=>$applications, 'leases'=>$leases, 'maintenances'=>$maintenances]);

            }
            return response()->json(['success' => false, 'error'=>'user_not_found'], 422);
        }
    }

    public function getUserDocuments()
    {
        if($this->user){
            $user = $this->userRepository->firstByParams(['id'=>$this->user->id]);
            if($user){
                $user_icon = collect($user->user_icon);
                $user_id_card = collect($user->id_card);

                foreach($user->comments as $comment){
                    if($comment->media->isNotEmpty()){
                        foreach ($comment->media as $media){
                            $comments_media[] = $media;
                        }
                    }
                }
                foreach($user->leases as $lease){
                    foreach ($lease->media as $media){
                        $leases_media[] = $media;
                    }
                    if($lease->report){
                        foreach ($lease->report->media as $media){
                            $report_media[] = $media;
                        }
                    }
                }

                return response()->json(['success' => true, 'documents'=> ['user_icon'=>$user_icon, 'id_card'=>$user_id_card, 'comments_media'=>$comments_media ?? [], 'leases_media'=>$leases_media ?? [], 'report_media'=>$report_media ?? []] ]);
            }
            return response()->json(['success' => false, 'error'=>'user_not_found'], 422);
        }
    }

    public function getAdminDocuments($request)
    {
        $request->validate(['with'=>['required', 'string'], 'filter_by_tenant_id' => ['nullable','int']]);
        $array = $request->with ? explode(',', $request->with) : [];
        $arrayWith = [];
        foreach ($array as $item){
            $arrayWith[] = trim($item);
        }
        if($this->user) {
            $lease_media = [];
            $comments_media = [];

            $user = $this->userRepository->firstByParams(['id' => $this->user->id]);
            if ($user) {
                if(!$request->filter_by_tenant_id){
                    //user_icon
                    if (in_array('admin_icon', $arrayWith)) {
                        $user_icon = collect($user->user_icon);
                    }
                    //id_card
                    if (in_array('admin_id_card', $arrayWith)) {
                        $user_id_card = collect($user->id_card);
                    }
                }

                //media from admin comments
                if (in_array('comments_media', $arrayWith)) {
                    if($request->filter_by_tenant_id) {
                        foreach ($user->comments as $comment) {
                            if($comment->user_id == $request->filter_by_tenant_id){
                                if ($comment->media->isNotEmpty()) {
                                    foreach ($comment->media as $media) {
                                        $comments_media[] = $media;
                                    }
                                }
                            }
                        }
                    }else{
                        foreach ($user->comments as $comment) {
                            if ($comment->media->isNotEmpty()) {
                                foreach ($comment->media as $media) {
                                    $comments_media[] = $media;
                                }
                            }
                        }
                    }
                }

                //tenants media
                    $tenant_media = [];
                    $searchParams = new LeaseSearchParams();
                    $searchParams->limit = 10000000;
                    if($request->filter_by_tenant_id){
                        $searchParams->filter_by_tenant_id = $request->filter_by_tenant_id;
                    }
                    $tenants = (new LeaseRepository())->getTenants($searchParams);

                //leases
                if (in_array('leases_media', $arrayWith)) {

                    if($request->filter_by_tenant_id) {
                        if($tenants->isNotEmpty()){
                            $leases = $tenants[0]->leases;
                            foreach ($leases as $lease){
                                if($lease->admin_id == $request->userObj->id){
                                    if ($lease->media->isNotEmpty()) {
                                        foreach ($lease->media as $media) {
                                            $lease_media[] = $media;
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        foreach ($user->admin_leases as $lease) {
                            if ($lease->media->isNotEmpty()) {
                                foreach ($lease->media as $media) {
                                    $lease_media[] = $media;
                                }
                            }
                        }
                    }
                }

                //leases
                if (in_array('report_media', $arrayWith)) {

                    if($request->filter_by_tenant_id) {
                        if($tenants->isNotEmpty()){
                            $leases = $tenants[0]->leases;
                            foreach ($leases as $lease){
                                if($lease->admin_id == $request->userObj->id){
                                    if ($lease->report) {
                                        foreach ($lease->report->media as $media) {
                                            $report_media[] = $media;
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        foreach ($user->admin_leases as $lease) {
                            if ($lease->report) {
                                foreach ($lease->report->media as $media) {
                                    $report_media[] = $media;
                                }
                            }
                        }
                    }
                }

                    foreach ($tenants as $tenant) {
                        if (in_array('tenants_maintenance', $arrayWith)) {
                            foreach ($tenant->maintenance as $maintenance) {
                                if($maintenance->property && $maintenance->property->admin && $maintenance->property->admin->id == $request->userObj->id){
                                    if ($maintenance->media->isNotEmpty()) {
                                        foreach ($maintenance->media as $media) {
                                            $tenant_media['maintenance_media'][] = $media;
                                        }
                                    }
                                }
                            }
                        }
                        if (in_array('tenants_user_icons', $arrayWith)) {
                            foreach ($tenant->user_icon as $u_icon) {
                                $tenant_media['user_icons'][] = $u_icon;
                            }
                        }

                        if (in_array('tenants_id_cards', $arrayWith)) {
                            foreach ($tenant->id_card as $id_card) {
                                $tenant_media['id_cards'][] = $id_card;
                            }
                        }

                        if (in_array('tenants_comments_media', $arrayWith)) {
                            foreach ($tenant->comments as $comment) {
                                if($comment->admin_id == $request->userObj->id){
                                    if ($comment->media->isNotEmpty()) {
                                        foreach ($comment->media as $media) {
                                            $tenant_media['comments_media'][] = $media;
                                        }
                                    }
                                }
                            }
                        }
                    }

                return response()->json(['success' => true,
                    'documents'=> [
                        'leases_media' => $lease_media,
                        'comments_media' => $comments_media,
                        'report_media'=> $report_media ?? [],
                        'admin_icon' => $user_icon ?? [],
                        'admin_id_card' => $user_id_card ?? [],
                        'tenants_media' => $tenant_media ?? []
                    ]
                ]);

            }
            return response()->json(['success' => false, 'error'=>'user_not_found'], 422);
        }
    }

    /* controller's methods END */

}
