<?php

namespace App\Domains\Users\Services\Validation;

use App\Domains\Users\Models\User;
use App\Support\Service\Validation\ValidationInterface;

class UserValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
//            'first_name' => ['nullable', 'regex:'.User::NAME_REGEX, 'string', 'max:255'],
//            'last_name' => ['nullable', 'string', 'max:255', 'alpha'],
            'first_name' => ['nullable', 'string', 'max:255'],
            'last_name' => ['nullable', 'string', 'max:255'],
            'gender'=> ['nullable', 'string', 'max:1'],
            'dob'=> ['nullable', 'date', 'date_format:Y-m-d'],
            'phone'=> User::PHONE_VALIDATION(false),
            'fax'=> ['nullable', 'string','max:255'],
        ];
    }

    public function addressRules(): array
    {
        return [
            'address'=>['nullable'],
            '*.country' => ['nullable', 'string'],
            '*.city' => ['nullable', 'string'],
            '*.street' => ['nullable', 'string'],
            '*.zip_code' => ['nullable', 'string']
        ];
    }

    public function filesRules(): array
    {
        return [
            'user_icon' => ['nullable','image', 'mimes:jpg,png,jpeg,gif,svg,webp,heic'],
            'id_card.*' => ['nullable', 'file'],
        ];
    }

    public function idCardRules(): array
    {
        return [
            'id'=>['required', 'int'],
            'id_card_verified'=> ['string', 'required', 'in:'.User::ID_CARD_VERIFIED.','.User::ID_CARD_VERIFICATION_FAILED.','.User::ID_CARD_WAITING_FOR_VERIFICATION]
        ];
    }

    public function newPasswordRules(): array
    {
        return [
            'password' => ['required', 'string'],
            'new_password' => ['required', 'string', 'regex:' . User::PASSWORD_REGEX, 'min:8', 'max:255'],
        ];
    }
}
