<?php
namespace App\Domains\Users\Services;

use App\Domains\Auth\Services\Verification\ProcessEmailVerification;
use App\Domains\Notifications\Models\Notification;
use App\Domains\Notifications\Services\NotificationService;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use App\Domains\Users\Services\Validation\UserValidation;
use Carbon\Carbon;

class UserService
{
    public $user;
    public $userEntity;
    public $userRepository;
    public $userValidation;
    public $emailVerification;

    public function __construct(
    ){
        $this->user = request()->userObj ?? auth('api')->user();
        if(!$this->user){
           return response()->json(['success' => false, 'error' => 'Unauthorized'], 401);
        }
        $this->userRepository = new ProductRepository();
        $this->userEntity = new User();
        $this->userValidation = new UserValidation();
        $this->emailVerification = new ProcessEmailVerification();
    }

    use UserTrait;

    public function changeUserData($request): \Illuminate\Http\JsonResponse
    {
        $data = $request->validate($this->userValidation->rules());
        $request->validate($this->userValidation->addressRules());
        $request->validate($this->userValidation->filesRules());

        if (empty($request->all())) {
            return response()->json(['success' => false, 'error' => 'empty_data'], 422);
        }

        $errors = $this->saveFiles($request, $this->user);

        if($request->hasFile('id_card') && $this->user->role === User::ROLE_USER){
//            $this->notifyRecipient();
        }
        $this->updateAddress($request, $this->user);
        $this->updateUser($data);

        $user = $this->userRepository->firstByParams(['id' => $this->user->id]);
        return response()->json(['success' => true, 'user' => $user, 'errors' => $errors]);
    }

    public function updateUser($data): void
    {
        if($this->user) {
            $data['id'] = $this->user->id;

            if($data['phone'] ?? ''){
                $data['phone_verified_at'] = null;
            }
            $this->userEntity::store($data);
        }
    }

    public function updatePassword($request)
    {
        if($this->user) {
            $request->validate($this->userValidation->newPasswordRules());
            if (!$request->token = auth('api')->attempt(['email'=>$this->user->email, 'password'=>$request->password], ['exp' => Carbon::now()->addHour()->timestamp])) {
                return response()->json(['success'=>false, 'error'=>"current_password_wrong", "message"=>"Invalid current password."], 422);
            }
            $this->userEntity::store(['id'=>$this->user->id, 'password'=>$request->new_password]);

            $user = $this->userRepository->firstByParams(['id'=>$this->user->id]);
            return response()->json(['success'=>true, 'user'=>$user]);
        }
    }

    public function changeEmail($request): \Illuminate\Http\JsonResponse
    {
        $request->validate(['email'=>'required']);
        if($this->user) {
            $user = $this->userRepository->firstByParams(['email'=>$request->email]);
            if($user){
                return response()->json(['success'=>false, 'error'=>'email_already_exist', "message"=>"Email already exists."], 422);
            }
            $this->emailVerification->send($request);
            return response()->json(['success'=>true]);
        }
        return response()->json(['success'=>false, 'error'=>"user_not_found", "message"=>"Unauthorized."], 401);
    }

    public function deleteUser(): \Illuminate\Http\JsonResponse
    {
        if($this->user){
            $this->userEntity::store(['id'=>$this->user->id, 'email_verified_at'=> null, 'active'=>false, 'deleted_at' => Carbon::now()]);
            auth('api')->logout();
            return response()->json(['success'=>true]);
        }
        return response()->json(['success'=>false, 'error'=>'Unauthorized'], 401);
    }

    public function getUser(): \Illuminate\Http\JsonResponse
    {
        if($this->user){
            $user = $this->userRepository->firstByParams(['id'=>$this->user->id]);
            return response()->json(['success' => true, 'user' => $user]);
        }

        return response()->json(['success' => false, 'error'=>'Unauthorized'], 401);
    }

    public function verifyIDCardByAdmin($request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->userValidation->idCardRules());
        $user = $this->userRepository->firstByParamsWithoutRelations(['id'=>$request->id]);
        if($user){
            $media = $user->getMedia('id_card')->first();
            if($media){
                $user->id_card_verified = $request->id_card_verified;
                $user->save();
                $title = $user->id_card_verified == User::ID_CARD_VERIFIED ? Notification::ID_CARD_VERIFIED_SUCCESSFULLY : Notification::ID_CARD_VERIFIED_FAILED;
                return $this->notifyRecipient($user, $title);
            }
            return response()->json(['success'=>false, 'error'=>'media_not_found', 'message'=>'The user does not have id_card media files, so id_card can not be verified.'], 422);
        }
        return response()->json(['success'=>false, 'error'=>'user_not_found', 'message'=>'The user was not found for the specified ID.'], 422);
    }

    public function getByID($request): \Illuminate\Http\JsonResponse
    {
        $request->validate(['id'=>['required', 'int'], 'with'=>['string']]);
        $with = [];
        if($request->with){
            $array = explode(',', $request->with);

            foreach ($array as $item){
                $with[] = trim($item);
            }
        }

        $user = $this->userRepository->firstByParamsWithSpecifiedRelations(['id'=>$request->id], $with);
        if(!$user){
            return response()->json(['success'=>false, 'error'=>'user_not_found', "message"=>"The user was not found for the specified ID."]);
        }
        return response()->json(['success'=>true, 'user'=>$user]);

    }

    public function notifyRecipient($user, $title): \Illuminate\Http\JsonResponse
    {

        $message = ['title' => $title, 'body' => null, 'entity'=>'user', 'entity_id'=>$user->id];

        $notificationService = new NotificationService($message, 'user', $user);
        $notificationService->saveNotification($user);
        $notification = $notificationService->sendNotification($user);

        if ($notification['success']) {
            return response()->json(['success' => true, 'user' => $user, 'notification' => $notification['response']]);
        }
        return response()->json(['success' => true, 'user' => $user, 'notification_error' => $notification['error']]);
    }
}
