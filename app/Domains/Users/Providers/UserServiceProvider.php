<?php

namespace App\Domains\Users\Providers;

use App\Support\Domain\ServiceProvider as DomainServiceProvider;

class UserServiceProvider extends DomainServiceProvider
{
    protected $resourceAlias = 'users';

    protected $hasViews = true;

    protected $hasTranslations = true;

    protected $hasMigrations = true;

    protected $factories = [

    ];

    protected $subProviders = [
        RouteServiceProvider::class
    ];
}
