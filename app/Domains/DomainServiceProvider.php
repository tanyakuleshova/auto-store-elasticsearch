<?php

namespace App\Domains;

use App\Domains\Auth\Providers\AuthServiceProvider;
use App\Domains\Basic\Providers\BasicServiceProvider;
use App\Domains\Media\Providers\MediaServiceProvider;
use App\Domains\Product\Providers\ProductServiceProvider;
use Illuminate\Support\AggregateServiceProvider;

class DomainServiceProvider extends AggregateServiceProvider
{
    /**
     * The provider class names.
     *
     * @var array
     */
    protected $providers = [
        BasicServiceProvider::class,
        ProductServiceProvider::class,
        AuthServiceProvider::class,
        MediaServiceProvider::class
    ];
}
