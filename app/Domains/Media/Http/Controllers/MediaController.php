<?php

namespace App\Domains\Media\Http\Controllers;

use App\Domains\Media\Services\ChunkingService;
use App\Domains\Media\Services\UploaderService;
use App\Support\Http\Controller as BaseController;
use Illuminate\Http\Request;
/**
 * @OA\PathItem(
 *      path="/api/media/upload"
 *  ),
 * @OA\Post(path="/api/media/upload",
 *     security={{ "apiAuth": {} }},
 *     tags={"media"},
 *     summary="Route to upload media",
 *     description="
 *     This endpoint uploads media for a specific entity.
 *     All mimes: jpg,png,jpeg,gif,svg,webp,heic,pdf,csv,doc,docx,odt,xls,xlsx.
 *     Image mimes: jpg,png,jpeg,gif,svg,webp,heic.
 *     Possible values of entity and collection parameters are on the table below.
 * <img width='60%' src='https://apidevseer.eivolo.com/swagger_images/media.jpg' />",
 *     @OA\RequestBody(
 *       @OA\MediaType(
 *       mediaType="multipart/form-data",
 *         @OA\Schema(
 *           @OA\Property(property="id",  description="ID of the entity. Required.", type="int"),
 *           @OA\Property(property="entity",  description="Name of the entity. Required.", type="string"),
 *           @OA\Property(property="collection",  description="Name of the media collection. Required.", type="string"),
 *           @OA\Property(property="files[]", description="Multiple files to upload. Required.", type="array", @OA\Items(type="file")),
 *        )
 *      )
 *    ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *       @OA\JsonContent(
 *          type="object",
 *          @OA\Property(property="success", type="bool", example=true),
 *          @OA\Property(property="url", type="string", example="http://domain/media/56/156/download.jpeg")
 *       )
 *     ),
 *     @OA\Response(response="422", description="validation error",
 *       @OA\JsonContent(
 *          type="object",
 *          @OA\Property(property="success", type="bool", example=false),
 *          @OA\Property(property="error", type="string", example="validation_error"),
 *          @OA\Property(property="errors", type="obj", example={ "id": {"The id must be a number."} })
 *       )
 *     ),
 *     @OA\Response(response="401", description="Unauthorized",
 *        @OA\JsonContent(
 *           type="object",
 *           @OA\Property(property="success", type="bool", example=false),
 *           @OA\Property(property="error", type="string", example="token_not_found")
 *        )
 *     )
 * ),
 *  @OA\PathItem(
 *      path="/api/media/user/upload"
 *  ),
 * @OA\Post(path="/api/media/user/upload",
 *     security={{ "apiAuth": {} }},
 *     tags={"media"},
 *     summary="Route to upload only user media.",
 *     description="
 *     This endpoint uploads media for a user entity.
 *     Allowed mimes: jpg,png,jpeg,gif,svg,webp,heic,pdf,csv,doc,docx,odt,xls,xlsx.
 *     Possible values of collection parameter: 'user_icon', 'id_card', 'images'.",
 *     @OA\RequestBody(
 *       @OA\MediaType(
 *       mediaType="multipart/form-data",
 *         @OA\Schema(
 *           @OA\Property(property="collection",  description="Possible values: 'user_icon', 'id_card', 'images'. Required.", type="string"),
 *           @OA\Property(property="files[]", description="Multiple files to upload. Required.", type="array", @OA\Items(type="file")),

 *        )
 *      )
 *    ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *       @OA\JsonContent(
 *          type="object",
 *          @OA\Property(property="success", type="bool", example=true),
 *          @OA\Property(property="url", type="string", example="http://domain/media/56/156/download.jpeg")
 *       )
 *     ),
 *     @OA\Response(response="422", description="wrong_collection",
 *       @OA\JsonContent(
 *          type="object",
 *          @OA\Property(property="success", type="bool", example=false),
 *          @OA\Property(property="error", type="string", example="wrong_collection"),
 *       )
 *     ),
 *     @OA\Response(response="401", description="Unauthorized",
 *        @OA\JsonContent(
 *           type="object",
 *           @OA\Property(property="success", type="bool", example=false),
 *           @OA\Property(property="error", type="string", example="token_not_found")
 *        )
 *     )
 * ),
 *  @OA\PathItem(
 *      path="/api/media/delete"
 *  ),
 *  @OA\Delete(path="/api/media/delete",
 *     security={{ "apiAuth": {} }},
 *     tags={"media"},
 *     summary="Route to delete media by id.",
 *     description="To delete a media file, pass the ID of media file.",
 *     @OA\Parameter(name="id", description="Specify the ID of the media file.", in="query"),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *       @OA\JsonContent(
 *          type="object",
 *          @OA\Property(property="success", type="bool", example=true)
 *       )
 *     ),
 *     @OA\Response(response="422", description="wrong_id",
 *       @OA\JsonContent(
 *          type="object",
 *          @OA\Property(property="success", type="bool", example=false),
 *          @OA\Property(property="error", type="string", example="wrong_id"),
 *          @OA\Property(property="message", type="string", example="The media file was not found by specified ID.")
 *       )
 *     ),
 *     @OA\Response(response="401", description="Unauthorized",
 *        @OA\JsonContent(
 *           type="object",
 *           @OA\Property(property="success", type="bool", example=false),
 *           @OA\Property(property="error", type="string", example="token_not_found")
 *        )
 *     )
 * ),
 * @OA\PathItem(
 *      path="/api/media/chunked_upload"
 *  ),
 * @OA\Post(path="/api/media/chunked_upload",
 *     security={{ "apiAuth": {} }},
 *     tags={"media"},
 *     summary="Route to upload huge media files.",
 *     description="
 *     Chunked upload is the process of breaking a file into smaller pieces, upload 'as is' and glue pieces into original file on the server side.
 *     The process includes several subsequent POST calls (transferring chunks).
 *     The endpoint returns the percentage of downloaded parts of the file. When the file download is complete, the path to the file is returned.
 *     Allowed mimes: jpg,png,jpeg,gif,svg,webp,heic,pdf,csv,doc,docx,odt,xls,xlsx.
 *     The max size of chunk is 5mb.",
 *     @OA\RequestBody(
 *       @OA\MediaType(
 *       mediaType="multipart/form-data",
 *         @OA\Schema(
 *           @OA\Property(property="file", description="Huge file to upload. Required.", type="file"),
 *        )
 *      )
 *    ),
 *     @OA\Response(
 *     response="200",
 *     description="successful operation",
 *       @OA\JsonContent(
 *          type="object",
 *          @OA\Property(property="success", type="bool", example=true),
 *          @OA\Property(property="done", type="int", example="67")
 *       )
 *     ),
 *     @OA\Response(response="401", description="Unauthorized",
 *        @OA\JsonContent(
 *           type="object",
 *           @OA\Property(property="success", type="bool", example=false),
 *           @OA\Property(property="error", type="string", example="token_not_found")
 *        )
 *     )
 * ),
 */
class MediaController extends BaseController
{
    public function __construct(
        protected UploaderService $service
    ){}

    public function uploadMedia(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->service->uploadMedia($request);
    }
    public function uploadUserMedia(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->service->uploadUserMedia($request);
    }
    public function deleteMedia(Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->service->deleteMedia($request);
    }

    public function chunkedUploading(Request $request, ChunkingService $chunkingService): \Illuminate\Http\JsonResponse
    {
        return $chunkingService->chunkedUploading($request);
    }

    public function chunkView()
    {
        return view('media::index');
    }

    public function chunk(Request $request)
    {
        if(!file_exists(storage_path("app/zip"))){
            mkdir(storage_path('/app/zip'),0777);
        }
        $filePath = storage_path('/app/zip');

        if (!file_exists($filePath)) {

            if (!mkdir($filePath, 0777, true)) {
                return response()->json(["ok"=>0, "info"=>"Failed to create $filePath"]);
            }
        }

        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
        $filePath = $filePath . DIRECTORY_SEPARATOR . $fileName;


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");

        if ($out) {
            $in = fopen($_FILES['file']['tmp_name'], "rb");

            if ($in) {
                while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
            } else {
                return response()->json(["ok"=>0, "info"=>'Failed to open input stream']);
            }

            fclose($in);
            fclose($out);
            unlink($_FILES['file']['tmp_name']);
        }

        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);
        }

        $info = "Upload OK";
        $ok =1;

        return response()->json(["ok"=>$ok, "info"=>$info]);
    }

}
