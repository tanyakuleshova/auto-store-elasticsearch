<?php

use App\Domains\Media\Http\Controllers\MediaController;
use Illuminate\Http\Request;
use League\Glide\Responses\LaravelResponseFactory;
use League\Glide\ServerFactory;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$this->router->get('/media/{folder}/{inner}/{image}', function (Request $request, $folder, $inner, $image) {

    if(!$request->w){
        $imagePath = storage_path('app/media')."/$folder/$inner/$image";
        if(!file_exists($imagePath)){
           return abort(404);
        }
        return response()->file($imagePath);
    }
    $server = ServerFactory::create([
        'response'=> new LaravelResponseFactory($request),
        'source' => storage_path('app'),
        'cache' => storage_path('glide/cache'),
    ]);

    try{
        return $server->getImageResponse($request->path(), $request->all());
    }catch (Exception $e){
        return response()->json(['success'=>false, 'error'=>$e->getMessage()]);
    }

});
$this->router->get('/media/{folder}/{inner}/{image}/download', function ($folder, $inner, $image) {

    $imagePath = storage_path('app/media')."/$folder/$inner/$image";
    return response()->download($imagePath);
});

$this->router->get('/upload/{f}/{d}/{n}', function (Request $request) {

    $server = ServerFactory::create([
        'response'=> new LaravelResponseFactory($request),
        'source' => storage_path('app'),
        'cache' => storage_path('glide/cache'),
    ]);

    try{
        return $server->getImageResponse($request->path(), $request->all());
    }catch (Exception $e){
        return response()->json(['success'=>false, 'error'=>$e->getMessage()]);
    }
});
$this->router->get('file-upload',[MediaController::class,'chunkView']) ;
$this->router->post('chunk',[MediaController::class,'chunk'])->name('chunk.store');
