<?php

namespace App\Domains\Media\Services\Validation;

use App\Domains\Media\Models\Media;
use App\Domains\Users\Models\User;
use App\Support\Service\Validation\ValidationInterface;

class UploaderValidation implements ValidationInterface
{
    public function rules(): array
    {
        return [
            'id' => ['required', 'int'],
            'entity' => ['required', 'string'],
            'collection'=> ['required', 'string'],
            'files.*'=>['required', 'file', 'mimes:'.MEDIA::ALL_MIMES]
        ];
    }
}
