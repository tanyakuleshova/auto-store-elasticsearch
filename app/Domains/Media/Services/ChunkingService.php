<?php
namespace App\Domains\Media\Services;
use App\Domains\Maintenance\Models\Maintenance;
use App\Domains\Media\Models\Media;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

class ChunkingService
{
    public function __construct(){}

    public function chunkedUploading($request): \Illuminate\Http\JsonResponse
    {
//        $request->validate(
//            ['file' => ['required',  'mimes:'.MEDIA::ALL_MIMES, 'max:'.Media::MAX_WEIGHT]],
//        );
//
//        var_dump($request->all());

        // create the file receiver
        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));

        // check if the upload is success, throw exception or return response you need
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }

        // receive the file
        $save = $receiver->receive();

        // check if the upload has finished (in chunk mode it will send smaller files)
        if ($save->isFinished()) {
            // save the file and return any response you need, current example uses `move` function. If you are
            // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
            return $this->saveFile($save->getFile());
        }

        // we are in chunk mode, lets send the current progress

        $handler = $save->handler();

        return response()->json([
            "done" => $handler->getPercentageDone(),
            'success' => true
        ]);
    }

    protected function saveFile(UploadedFile $file): \Illuminate\Http\JsonResponse
    {

        $fileName = $this->createFilename($file);
        // Group files by mime type
        $mime = str_replace('/', '-', $file->getMimeType());
        // Group files by the date (week
        $dateFolder = date("Y-m-W");

        // Build the file path
        $filePath = "upload/{$mime}/{$dateFolder}/";
        $finalPath = storage_path("app/".$filePath);

        // move the file name
        $file->move($finalPath, $fileName);

        return response()->json([
            'path' => env('APP_URL')."/".$filePath.$fileName,
            'mime_type' => $mime,
            'success' => true
        ]);
    }

    protected function createFilename(UploadedFile $file): string
    {
        $extension = $file->extension();
        $filename = str_replace(".".$extension, "", $file->getClientOriginalName()); // Filename without extension

        // Add timestamp hash to name of the file
        $filename .= "_" . md5(time()) . "." . $extension;

        return $filename;
    }

}
