<?php
namespace App\Domains\Media\Services;

use App\Domains\Comments\Models\Comment;
use App\Domains\Comments\Repository\SQLRepository\CommentRepository;
use App\Domains\Leases\Models\Lease;
use App\Domains\Leases\Repository\SQLRepository\LeaseRepository;
use App\Domains\Maintenance\Models\Maintenance;
use App\Domains\Maintenance\Repository\SQLRepository\MaintenanceRepository;
use App\Domains\Media\Models\Media;
use App\Domains\Media\Repository\SQLRepository\MediaRepository;
use App\Domains\Media\Services\Validation\UploaderValidation;
use App\Domains\Properties\Models\Room;
use App\Domains\Properties\Repository\SQLRepository\RoomRepository;
use App\Domains\Users\Models\User;
use App\Domains\Users\Repository\SQLRepository\ProductRepository;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;


class UploaderService
{

    protected $validation;
    protected $userRepository;
    protected $mediaRepository;

    public function __construct(){
        $this->validation = new UploaderValidation();
        $this->userRepository = new ProductRepository();
        $this->mediaRepository = new MediaRepository();
    }
    public function uploadMedia($request): \Illuminate\Http\JsonResponse
    {
        $request->validate($this->validation->rules());
        $repository = $this->getRepository($request->entity);
        if ($repository) {
            $entity = $repository->firstByParams(['id' => $request->id]);
            if (!$entity) {
                return response()->json(['success' => false, 'error' => 'wrong_id']);
            }
            if ($this->checkCollectionName($request->entity, $request->collection)) {
                try {
                    $this->uploadFilesFromRequest($entity, $request->collection, 'files');
                    $this->checkIDCard($request);
                } catch (\Exception $e) {
                    return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
                }
                return response()->json(['success' => true, 'url' =>  $this->media]);
            }
            return response()->json(['success' => false, 'error' => 'wrong_collection'], 422);
        }
        return response()->json(['success' => false, 'error' => 'wrong_entity'], 422);
    }

    public function uploadUserMedia($request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'files.*'=>['required', 'file', 'mimes:'.MEDIA::ALL_MIMES],
            'collection'=>['string', 'required']
        ]);

        $user = $this->userRepository->firstByParams(['id'=>auth('api')->id()]);
        if($user){
            if ($this->checkCollectionName('user', $request->collection)) {
                try {
                    $this->uploadFilesFromRequest($user, $request->collection, 'files');
                    $this->checkIDCard($request);
                } catch (\Exception $e) {
                    return response()->json(['success' => false, 'error' => $e->getMessage()], 422);
                }
                return response()->json(['success' => true, 'url' => $this->media]);
            }
            return response()->json(['success' => false, 'error' => 'wrong_collection'], 422);
        }
        return response()->json(['success' => false, 'error' => 'user_not_found'], 422);
    }

    public function deleteMedia($request): \Illuminate\Http\JsonResponse
    {
        $request->validate(['id'=> 'int|required']);
        $user = auth('api')->user();
        $user_id = $user->id;
        $media = $this->mediaRepository->firstByParams(['id'=>$request->id]);
        if(!$media){
            return response()->json(['success' => false, 'error'=>'wrong_media_id', 'message'=>'The media file was not found by specified ID.'], 422);
        }

        //check if user can delete this media
        $model = new $media->model_type;

        if($media->model_type == 'App\Domains\Users\Models\User'){
            if(($user_id !== $media->model_id) && ($user->role !== User::ROLE_ADMIN)){
                return response()->json(['success' => false, 'error'=>'permission_denied', 'message'=>'The media found by the specified ID is associated with another user.'], 422);
            }
        }else{
            $entity = $model::where('id',  $media->model_id)->first();
            if($entity){
                if($entity->user_id){
                    if(($entity->user_id !== $user_id)){
                        return response()->json(['success' => false, 'error'=>'permission_denied', 'message'=>'The media found by the specified ID is associated with another user.'], 422);
                    }
                }
                if($entity->admin_id){
                    if(($entity->admin_id !== $user_id)){
                        return response()->json(['success' => false, 'error'=>'permission_denied', 'message'=>'The media found by the specified ID is associated with another user.'], 422);
                    }
                }
            }
        }

        //delete media
        $media->delete();
        return response()->json(['success' => true]);
    }

    public function uploadFilesFromRequest($entity, $collection, $parameter): array
    {
        $this->media = [];
        $entity->addMultipleMediaFromRequest([$parameter])
            ->each(function ($fileAdder) use ($collection) {
                $media = $fileAdder->toMediaCollection($collection);
                $path = storage_path() . "/app/media" . $media->partOfPath();
                try{
                    Image::load($path)->fit(Manipulations::FIT_MAX, Media::MAX_WIDTH, Media::MAX_HEIGHT)->save($path);
                }catch (\Exception $e){

                }
                $this->media[] = $media->path();
            });
        return $this->media;
    }

    public function getRepository($entity)
    {
        $repositories = [
            'user' => (new ProductRepository),
            'maintenance' => (new MaintenanceRepository),
            'comment' => (new CommentRepository),
            'room' => (new RoomRepository),
            'lease' => (new LeaseRepository),
        ];
        if (isset($repositories[$entity])) {
            return $repositories[$entity];
        }
        return false;
    }

    public function checkCollectionName($entity, $collection): bool
    {
        $collections = [
            'user' => User::mediaCollections(),
            'maintenance' => Maintenance::mediaCollections(),
            'comment' => Comment::mediaCollections(),
            'room' => Room::mediaCollections(),
            'lease' => Lease::mediaCollections(),
        ];
        if (isset($collections[$entity])) {
            if (in_array($collection, $collections[$entity])) {
                return true;
            }
        }
        return false;
    }

    public function checkIDCard($request): void
    {
        if($request->collection == "id_card") {
            $id = $request->id ?? auth('api')->id();
            $user = $this->userRepository->firstByParams(['id'=>$id]);
            if($user) {
                $user->id_card_verified = User::ID_CARD_WAITING_FOR_VERIFICATION;
                $user->save();
            }
        }
    }
}
