<?php

namespace App\Domains\Media\Providers;

use App\Support\Domain\ServiceProvider as DomainServiceProvider;

class MediaServiceProvider extends DomainServiceProvider
{
    protected $resourceAlias = 'media';

    protected $hasViews = true;

    protected $hasTranslations = false;

    protected $hasMigrations = true;

    protected $factories = [

    ];

    protected $subProviders = [
        RouteServiceProvider::class
    ];
}
