<?php

namespace App\Domains\Media\Jobs;

use App\Domains\SmsProvider\Services\SendSms\SendSMSTwilio;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class MediaCacheJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $phone;
    public $message;

    /**
     * Execute the job.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle()
    {
        try{
           Artisan::call('clean_media_cache');
        }catch (\Exception $error){
            Log::channel('errors')
                ->info("time: " . Carbon::now()->format('Y-m-d H:i:s')
                    .", error: ". $error->getMessage());
        }
    }
}
