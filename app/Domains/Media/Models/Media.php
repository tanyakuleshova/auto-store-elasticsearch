<?php

namespace App\Domains\Media\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
/**
 * @OA\Schema(
 * @OA\Property(property="id", type="int",  description="ID of the media.", readOnly="true"),
 * @OA\Property(property="model_id", type="int",  description="ID of the entity to which media is related.", readOnly="false"),
 * @OA\Property(property="collection_name", type="string",description="Name of the media collection. Entities can have multiple media collections. For example, a user has 'user_icon', 'id_card' and other collections.", readOnly="false"),
 * @OA\Property(property="file_name", type="string",description="Name of the media file.", readOnly="true"),
 * @OA\Property(property="size", type="int",description="Size (in bytes) of the media file. ", readOnly="true"),
 * @OA\Property(property="order_column", type="int",description="Order of the media file.", readOnly="true"),
 * @OA\Property(property="original_url", type="string",description="Url of the original size of media.", readOnly="true"),
 * @OA\Property(property="xs_url", type="string",description="Url of small size (width 300px) of the media file (only for images, for other files its empty string).", readOnly="true"),
 * @OA\Property(property="md_url", type="string",description="Url of medium size (width 600px) of the media file (only for images, for other files its empty string).", readOnly="true"),
 * @OA\Property(property="lg_url", type="string",description="Url of large size (width 1200px) of the media file (only for images, for other files its empty string).", readOnly="true"),
 * )
 *
 * @package App\Models
 */
class Media extends \Spatie\MediaLibrary\MediaCollections\Models\Media
{
    protected $hidden = [
        'model_type', 'uuid', 'disk', 'conversions_disk', 'manipulations',
        'created_at', 'updated_at', 'custom_properties',
        'responsive_images', 'preview_url', 'name', 'mime_type', 'generated_conversions'
    ];
    const ALL_MIMES = 'jpg,png,jpeg,gif,svg,webp,heic,pdf,csv,doc,docx,odt,xls,xlsx';
    const IMAGE_MIMES = 'jpg,png,jpeg,gif,svg,webp,heic';
    const FILE_MIMES = 'pdf,csv,doc,docx,odt,xls,xlsx';

    const MAX_WIDTH = 3840;
    const MAX_HEIGHT = 2160;
    const MAX_CACHE_SIZE = 10000;
    const MAX_WEIGHT = 5000;

    public static function getCachePath(): string
    {
        return storage_path().'/glide/cache';
    }

    protected $appends = ['original_url', 'xs_url', 'md_url', 'lg_url'];

    public function is_image(): bool
    {
        $arr = explode('.', $this->file_name);
        $ext = end($arr);
        $mimes = explode(',',self::IMAGE_MIMES);
        if(in_array($ext, $mimes)){
            return true;
        }
        return false;
    }
    public function getXsUrlAttribute(): string
    {
        $arr = explode('.', $this->file_name);
        $ext = end($arr);
        if($ext === 'svg'){
            return $this->path();
        }
        return $this->is_image() ? $this->path().'?w=300' : "";
    }
    public function getMdUrlAttribute(): string
    {
        $arr = explode('.', $this->file_name);
        $ext = end($arr);
        if($ext === 'svg'){
            return $this->path();
        }
        return $this->is_image() ? $this->path().'?w=600' : "";
    }
    public function getLgUrlAttribute(): string
    {
        $arr = explode('.', $this->file_name);
        $ext = end($arr);
        if($ext === 'svg'){
            return $this->path();
        }
        return $this->is_image() ? $this->path().'?w=1200' : "";
    }

    public function generateSize()
    {

    }
    protected function originalUrl(): Attribute
    {

        return Attribute::get(fn () => $this->path());
    }

    public function path(): string
    {
        $folder = str_pad(substr($this->getKey(), -2), 2, '0', STR_PAD_LEFT);
        return config('app.url')."/".config('project.media_folder')."/".$folder .'/'.$this->getKey().'/'.$this->file_name;

    }
    public function partOfPath(): string
    {
        $folder = str_pad(substr($this->getKey(), -2), 2, '0', STR_PAD_LEFT);
        return "/".$folder .'/'.$this->getKey().'/'.$this->file_name;
    }
}
