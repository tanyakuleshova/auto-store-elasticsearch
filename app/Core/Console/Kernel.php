<?php

namespace App\Core\Console;

use App\Console\Commands\ScheduleReport;
use App\Domains\Leases\Jobs\UpdateLeaseStatus;
use App\Domains\Media\Jobs\MediaCacheJob;
use App\Domains\Properties\Console\Commands\OpenHomeReminder;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    use \App\Core\Console\Traits\ExposeBehaviors;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Support\Console\Commands\Seed'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        $this->load(__DIR__.'../../Support/Console/Commands');

        require base_path('routes/console.php');
    }
}
