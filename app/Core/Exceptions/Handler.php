<?php

namespace App\Core\Exceptions;

use App\Domains\Notifications\Services\Notify;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Elastic\Transport\Exception\NoNodeAvailableException;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
    public function render($request, Throwable $e)
    {
        $parent = parent::render($request, $e);
        if($e instanceof ValidationException){
            $content = json_decode($parent->getContent(), true);
            $content['success'] = false;
            $content['error'] = 'validation_error';
            $parent->setContent(json_encode($content));
        }
        if($e instanceof NotFoundHttpException){
            return response()->json(['success'=>false, 'error'=>'route_not_found'], 404);
        }
        if($e instanceof MethodNotAllowedHttpException){
            return response()->json(['success'=>false, 'error'=>$e->getMessage()], 404);
        }
        if($e instanceof NoNodeAvailableException){
            $message = "\n \nPossible ways to fix: docker-compose logs elasticsearch; docker-compose restart elasticsearch";
            Notify::send('Slack', "Error: ".$e->getMessage(). $message);
        }

        if($e instanceof ClientResponseException){
//            if(str_contains("index_not_found_exception", $e->getMessage())){
                $message = "\n \nHow to fix: In case of 'index not found' run: docker-compose update_data";
                Notify::send('Slack', "Error: ".$e->getMessage(). $message);
//            }
        }
        if($e instanceof RelationNotFoundException){
            return response()->json(['success'=>false, 'error'=>'invalid_relationship'], 422);
        }

        if($e->getMessage() == "Method name must be a string"){
            return response()->json(['success'=>false, 'error'=>'invalid_relationship'], 422);
        }


        return $parent;

    }
}
