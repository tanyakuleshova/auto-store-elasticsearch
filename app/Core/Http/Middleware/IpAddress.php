<?php

namespace App\Core\Http\Middleware;


use App\Domains\Auth\Traits\IsExpired;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\DB;


class IpAddress
{
    use IsExpired;
    protected const MAX_ATTEMPTS = 10;
    protected const PERIOD = 3600; //1 hour

    public function handle($request, Closure $next)
    {
//        $request->headers->set('X-Requested-With', 'XMLHttpRequest');


//        $ip = $_SERVER['REMOTE_ADDR'] ?? '';
//        $ip = $request->ip() ?? '';
//        $ip = $request->getClientIp() ?? '';
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'] ?? "";

        if($ip && !$request->test){
            $item = \App\Domains\Auth\Models\IpAddress::where('ip', $ip)->first();
            if($item){
                if($this->is_active($item, self::PERIOD)){
                    if($item->attempts < self::MAX_ATTEMPTS){
                        $item->attempts += 1;

                        $item->save();
                        return $next($request);
                    }else{
                        return response(['success'=>false, 'error'=>'too_many_requests_per_hour',
                            'message'=>"You sent too many requests. Please, try later."], 422);
                    }
                }else{
                    DB::table('ip_addresses')->where('ip', $ip)->delete();
                }
            }
            $item = new \App\Domains\Auth\Models\IpAddress();
            $item->ip = $ip;
            $item->attempts = 1;
            $item->save();
        }

//        else{
//            return response(['success'=>false, 'error'=>'ip_not_identified'], 422);
//        }
        return $next($request);
    }
}
