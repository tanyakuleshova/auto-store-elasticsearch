<?php

namespace App\Core\Http\Middleware;

use Closure;
use Exception;

use Tymon\JWTAuth\Facades\JWTAuth;

class JwtMiddleware
{

    public function handle($request, Closure $next)
    {
        try {
           $u = JWTAuth::parseToken()->authenticate();
            $request->userObj = $u;
//           $payload = auth('api')->payload();
//           if(isset($payload['exp'])){
//
//               $now = time();
//               if($payload['exp'] > $now){
//                   $response = $next($request);
//                   $token = JWTAuth::getToken();
//                   $newToken = JWTAuth::setToken($token)->refresh();
//                   JWTAuth::setToken($newToken)->authenticate();
//                   $response->header('Authorization', 'Bearer '.$newToken);
//                   return $response;
//               }
//           }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['success'=>false, 'error' => 'token_is_invalid'], 401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['success'=>false, 'error' => 'token_is_expired'], 401);
            }else{
                return response()->json(['success'=>false, 'error' => 'token_not_found'], 401);
            }
        }
        return $next($request);
    }
}
