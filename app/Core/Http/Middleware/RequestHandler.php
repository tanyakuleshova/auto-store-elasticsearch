<?php

namespace App\Core\Http\Middleware;


use App\Domains\Auth\Traits\IsExpired;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\DB;


class RequestHandler
{
    public function handle($request, Closure $next)
    {
        $request->headers->set('X-Requested-With', 'XMLHttpRequest');
        return $next($request);
    }
}
