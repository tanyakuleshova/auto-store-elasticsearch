<?php

namespace App\Core\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;

class MailerType
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
