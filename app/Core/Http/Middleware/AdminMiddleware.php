<?php

namespace App\Core\Http\Middleware;

use App\Domains\Users\Models\User;
use Closure;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();

            if($user){
                $request->userObj = $user;
                if($user->role != User::ROLE_ADMIN){
                    return response()->json(['success'=>false, 'error'=>'Permission denied.'], 401);
                }
            }else{
                return response()->json(['success'=>false, 'error'=>'Unauthorized'], 401);
            }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['success'=>false, 'error' => 'token_is_invalid'], 401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['success'=>false, 'error' => 'token_is_expired'], 401);
            }else{
                return response()->json(['success'=>false, 'error' => 'token_not_found'], 401);
            }
        }
        return $next($request);
    }
}
