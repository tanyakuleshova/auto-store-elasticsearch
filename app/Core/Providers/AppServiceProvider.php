<?php

namespace App\Core\Providers;

use Illuminate\Support\Facades\URL;
use PDO;
use Illuminate\Support\ServiceProvider;
use Illuminate\Mail\MailServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(app_path('Support/Helpers').DIRECTORY_SEPARATOR.'*.php') as $filename) {
            require_once $filename;
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(config('app.env') !== 'local') {
            URL::forceScheme('https');
        }
    }

    public function registerHelpers()
    {
        foreach (glob(app_path('Support/Helpers').DIRECTORY_SEPARATOR.'*.php') as $filename) {
            require_once $filename;
        }
    }
}
