<?php

return [
    'host' => env('ELASTICSEARCH_HOST'),
    'user' => '',
    'password' => '',
    'cloud_id' => '',
    'api_key' => '',
    'indices' => [
        'mappings' => [
            'basic' => [
                'properties' => [
                    'id' => [
                        'type' => 'keyword',
                    ],
                    'name' => [
                        'type' => 'text'
                    ],
                    'content' => [
                        'type' => 'text'
                    ],
                ],
            ],
            'properties' => [

            ],

            'applications'=>[]
        ],
        'settings' => [
            'default' => [
                'number_of_shards' => 1,
                'number_of_replicas' => 0,
            ],
        ]
    ],
];
