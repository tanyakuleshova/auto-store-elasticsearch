<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    // to call all DatabaseSeeder.php from Domains components run:
    // php artisan db:seed

    // to call seeder from specific domain run:
    // php artisan db:seed --domain=Users

    public function run($class='')
    {
        if($class){
            $this->call($class);
        }

        //if you don't want to create seeder inside Domains component
        //put your seeder's class below:
    }
}
